var express       = require('express'),
    cookieSession = require('cookie-session'),
    http          = require('http'),
    port          = process.env.PORT || 3000,
    request       = require('request'),
    qs            = require('querystring'),
    util          = require('util');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser'),
    QuickBooks    = require('./index');

// all Routes
var routes              = require('./routes/index');
var users               = require('./routes/users'),
    invoice_bill        = require('./routes/invoice/purchase/bill_invoice'),
    credit_memo         = require('./routes/invoice/credit/credit_memo'),
    // time_activity       = require('./routes/invoice/deliveryNote/time_activity'),
    ovpm_payment        = require('./routes/payment/ovpm_payment'),
    ovpm_billpayment    = require('./routes/payment/ovpm_billpayment'),
    journalEntry        = require('./routes/payment/journalEntry'),
    vendor              = require('./routes/master/vendor'),
    customer            = require('./routes/master/customer'),
    employee            = require('./routes/master/employee'),
    agedReceivablesDetail = require('./routes/reports/agedReceivablesDetail'), 
    agedPayableDetail = require('./routes/reports/agedPayableDetail'), 
    // CustomerBalanceDetail      = require('./routes/reports/CustomerBalanceDetail'),
    //invoices            = require('./routes/invoice'),
    //ovpms_sales         = require('./routes/sales/ovpm_payment'),
   // ovpms_purchase      = require('./routes/sales/ovpm'),
   // invoices_purchase   = require('./routes/purchase/bill_invoice'),
    opch   = require('./routes/invoice/sales/opch');
    
    

var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
    tmpEmployeeInfo = {'key':'value'},
    jsonObj = {'key':'value'},
    columnWidth = 30;

app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser('brad'))
app.use(cookieSession({name: 'session', keys: ['key1']}))
var engines = require('consolidate');

app.set('views', path.join(__dirname, 'views'));
//app.engine('jade', engines.jade);
//app.engine('html', engines.ejs);

app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//testing
app.use('/', routes);
// app.use('/time_activity', time_activity);
app.use('/credit_memo', credit_memo);
app.use('/bill_invoice', invoice_bill);
app.use('/ovpm_payment', ovpm_payment);
app.use('/ovpm_billpayment', ovpm_billpayment);
app.use('/journalEntry', journalEntry);
// master pages
app.use('/customer', customer);
app.use('/vendor', vendor);
app.use('/employee', employee);
// app.use('/ovpm_payment', ovpms_purchase);
app.use("/agedReceivablesDetail", agedReceivablesDetail);
app.use("/agedPayableDetail", agedPayableDetail);
app.use('/opch', opch);
//app.use('/users', users);




// creating excel file


// // error handlers
// //catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// // development error handler
// // will print stacktrace
// if (app.get('env') === 'development') {
//   app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//       message: err.message,
//       error: err
//     });
//   });
// }

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//   res.status(err.status || 500);
//   res.render('error', {
//     message: err.message,
//     error: {}
//   });
// });



module.exports = app;

