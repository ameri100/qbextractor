var express = require('express'),
    request = require('request'),
    cookieSession = require('cookie-session'),
    http = require('http'),
    port = process.env.PORT || 3000,
    request = require('request'),
    qs = require('querystring'),
    util = require('util');
var path = require('path');
var router = express.Router(),
    QuickBooks = require('../../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
    tmpEmployeeInfo = {
        'key': 'value'
    },
    jsonObj = {
        'key': 'value'
    },
    jsonObjTerm = {
        'key': 'value'
    },
    columnWidth = 30;
var terms = {
    "": ""
};


// putting all invoice data into excel file (Invoice Items)  HEADER
router.post("/getPurchaseInvoice", function(req, res) {

    var options = {
        useStyles: true,
        useSharedStrings: true
    };


    var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    workbook.zip.pipe(res);
    var worksheet_opch = workbook.addWorksheet("OPCH");
    var worksheet_pch1 = workbook.addWorksheet("PCH1");

    var billInvoiceHeader = {
        DocNum: "DocNum",
        DocType: "DocType",
        DocDate: "DocDate",
        DocDueDate: "DocDueDate",
        CardCode: "CardCode",
        Address: "Address",
        NumAtCard: "NumAtCard",
        Comments: "Comments",
        JournalMemo: "JournalMemo",
        PaymentGroupCode: "PaymentGroupCode",
        SalesPersonCode: "SalesPersonCode",
        TaxDate: "TaxDate",
        Address2: "Address2",
        ControlAccount: "ControlAccount",
        U_OrigEntry: "U_OrigEntry",
        U_OrigNum: "U_OrigNum"
    };

    worksheet_opch.columns = [{
        header: billInvoiceHeader.DocNum,
        key: billInvoiceHeader.DocNum,
        width: columnWidth
    }, {
        header: billInvoiceHeader.DocType,
        key: billInvoiceHeader.DocType,
        width: columnWidth
    }, {
        header: billInvoiceHeader.DocDate,
        key: billInvoiceHeader.DocDate,
        width: columnWidth
    }, {
        header: billInvoiceHeader.DocDueDate,
        key: billInvoiceHeader.DocDueDate,
        width: columnWidth
    }, {
        header: billInvoiceHeader.CardCode,
        key: billInvoiceHeader.CardCode,
        width: columnWidth
    }, {
        header: billInvoiceHeader.Address,
        key: billInvoiceHeader.Address,
        width: columnWidth
    }, {
        header: billInvoiceHeader.NumAtCard,
        key: billInvoiceHeader.NumAtCard,
        width: columnWidth
    }, {
        header: billInvoiceHeader.Comments,
        key: billInvoiceHeader.Comments,
        width: columnWidth
    }, {
        header: billInvoiceHeader.JournalMemo,
        key: billInvoiceHeader.JournalMemo,
        width: columnWidth
    }, {
        header: billInvoiceHeader.PaymentGroupCode,
        key: billInvoiceHeader.PaymentGroupCode,
        width: columnWidth
    }, {
        header: billInvoiceHeader.SalesPersonCode,
        key: billInvoiceHeader.SalesPersonCode,
        width: columnWidth
    }, {
        header: billInvoiceHeader.TaxDate,
        key: billInvoiceHeader.TaxDate,
        width: columnWidth
    }, {
        header: billInvoiceHeader.Address2,
        key: billInvoiceHeader.Address2,
        width: columnWidth
    }, {
        header: billInvoiceHeader.ControlAccount,
        key: billInvoiceHeader.ControlAccount,
        width: columnWidth
    }, {
        header: billInvoiceHeader.U_OrigEntry,
        key: billInvoiceHeader.U_OrigEntry,
        width: columnWidth
    }, {
        header: billInvoiceHeader.U_OrigNum,
        key: billInvoiceHeader.U_OrigNum,
        width: columnWidth
    }];

    var purchaseInvoiceItem = {
        ParentKey: "ParentKey",
        LineNum: "LineNum",
        ItemCode: "ItemCode",
        Quantity: "Quantity",
        Price: "Price",
        DiscountPercent: "DiscountPercent",
        WarehouseCode: "WarehouseCode",
        TaxCode: "TaxCode",
        WTLiable: "WTLiable",
        LineTotal: "LineTotal",
        UnitPrice: "UnitPrice",
        U_OrigEntry: "U_OrigEntry",
        U_OrigLine: "U_OrigLine",
        Description: "Description"
    };

    worksheet_pch1.columns = [{
        header: purchaseInvoiceItem.ParentKey,
        key: purchaseInvoiceItem.ParentKey,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.LineNum,
        key: purchaseInvoiceItem.LineNum,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.ItemCode,
        key: purchaseInvoiceItem.ItemCode,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.Quantity,
        key: purchaseInvoiceItem.Quantity,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.Price,
        key: purchaseInvoiceItem.Price,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.DiscountPercent,
        key: purchaseInvoiceItem.DiscountPercent,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.WarehouseCode,
        key: purchaseInvoiceItem.WarehouseCode,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.TaxCode,
        key: purchaseInvoiceItem.TaxCode,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.WTLiable,
        key: purchaseInvoiceItem.WTLiable,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.LineTotal,
        key: purchaseInvoiceItem.LineTotal,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.UnitPrice,
        key: purchaseInvoiceItem.UnitPrice,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.U_OrigEntry,
        key: purchaseInvoiceItem.U_OrigEntry,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.U_OrigLine,
        key: purchaseInvoiceItem.U_OrigLine,
        width: columnWidth
    }, {
        header: purchaseInvoiceItem.Description,
        key: purchaseInvoiceItem.Description,
        width: columnWidth
    }];

    qbo.findTerms('', function(err1, term) {
        var obj = {
            'key': 'value'
        };
        jsonObjTerm = term;
    });

    qbo.findBills([{
        field: 'TxnDate',
        value: '2017-08-01',
        operator: '>='
    }, {
        field: 'TxnDate',
        value: '2017-09-30',
        operator: '<='
    }, {
        field: 'limit',
        value: 900
    }], function(err, bills) {
        jsonObj = bills;

        var key = 'Bill';
        console.log(jsonObj);
        var invoices = {
            "": ""
        };
        invoices = jsonObj.QueryResponse[key];
        //console.log('number of bills', invoices.length);
        var docnumber = 1000;
        //console.log("Term object Length", jsonObjTerm);
        for (var i = 0; i < invoices.length; i++) {
            var vendorType = invoices[i]["VendorRef"];
            // console.log(invoices[i].SalesTermRef.value);

            docnumber = docnumber + 1;
            if (invoices[i].hasOwnProperty("SalesTermRef")) {
                //map the docDueDate from the Terms API
                terms = jsonObjTerm.QueryResponse['Term'];
                for (var t = terms.length - 1; t >= 0; t--) {

                    var paymentGroupCode = ''
                        //var tmpDocDueDays = new Date();
                    if (terms[t].Id == invoices[i].SalesTermRef.value) {
                        paymentGroupCode = terms[t].Name;

                        worksheet_opch.addRow({
                            // JournalMemo data not found in QuickBooks
                            DocNum: docnumber,
                            DocType: "dDocument_Items",
                            DocDate: invoices[i]["TxnDate"],
                            DocDueDate: invoices[i]["DueDate"],
                            CardCode: vendorType.name,
                            Address: "",
                            NumAtCard: invoices[i]["DocNumber"],
                            Comments: invoices[i]["PrivateNote"],
                            JournalMemo: 'Opening Balance',
                            PaymentGroupCode: paymentGroupCode,
                            SalesPersonCode: "",
                            TaxDate: invoices[i]["TxnDate"],
                            Address2: "",
                            ControlAccount: "",
                            U_OrigEntry: invoices[i]["Id"],
                            U_OrigNum: invoices[i]["DocNumber"]
                        }).commit;
                    }
                }
            } else {
                worksheet_opch.addRow({
                    DocNum: docnumber,
                    DocType: "dDocument_Items",
                    DocDate: invoices[i]["TxnDate"],
                    DocDueDate: invoices[i]["DueDate"],
                    CardCode: vendorType.name,
                    Address: "",
                    NumAtCard: invoices[i]["DocNumber"],
                    Comments: invoices[i]["PrivateNote"],
                    JournalMemo: 'Opening Balance',
                    PaymentGroupCode: paymentGroupCode,
                    SalesPersonCode: "",
                    TaxDate: invoices[i]["TxnDate"],
                    Address2: "",
                    ControlAccount: "",
                    U_OrigEntry: invoices[i]["Id"],
                    U_OrigNum: invoices[i]["DocNumber"]
                }).commit;
            }
            var Line_key = 'Line';
            var LineItems = invoices[i][Line_key];
            for (var k = 0; k < LineItems.length; k++) {
                //console.log('Length of Line Item ',k);
                var detailType = LineItems[k]["DetailType"];
                //console.log(detailType);
                if (detailType.toString() === "ItemBasedExpenseLineDetail") {
                    var PurchaseItem = LineItems[k]["ItemBasedExpenseLineDetail"];
                    var itemCode = PurchaseItem.ItemRef.name;
                    var price = PurchaseItem["UnitPrice"];
                    var Qnty = PurchaseItem["Qty"];
                } else if (detailType.toString() === "AccountBasedExpenseLineDetail") {
                    var PurchaseItem = LineItems[k]["AccountBasedExpenseLineDetail"];
                    var itemCode = PurchaseItem.AccountRef.name;
                    var price = "";
                    var Qnty = "";
                }
                worksheet_pch1.addRow({
                    // JournalMemo data not found in QuickBooks
                    ParentKey: docnumber,
                    LineNum: '',
                    ItemCode: itemCode,
                    Quantity: Qnty,
                    Price: price,
                    DiscountPercent: '0',
                    WarehouseCode: '',
                    TaxCode: '',
                    WTLiable: 'tNo',
                    LineTotal: LineItems[k].Amount,
                    UnitPrice: price,
                    U_OrigEntry: invoices[i].Id,
                    U_OrigLine: LineItems[k].Id,
                    Description: LineItems[k].Description
                }).commit;
            };
        }

        worksheet_opch.commit();
        worksheet_pch1.commit();
        workbook.commit();
    });
});
module.exports = router;