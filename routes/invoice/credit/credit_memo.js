var express = require('express'),
    request = require('request'),
    cookieSession = require('cookie-session'),
    http = require('http'),
    port = process.env.PORT || 3000,
    request = require('request'),
    qs = require('querystring'),
    util = require('util');
var path = require('path');
var router = express.Router(),
    QuickBooks = require('../../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = ''
jsonObj = {
        'key': 'value'
    },
    jsonObjItem = {
        'key': 'value'
    },
    columnWidth = 30;
var credits = {
    "": ""
};

// putting all invoice data into excel file (CreditMemo Header) OPCH
router.post("/getCredit", function(req, res) {

    var options = {
        useStyles: true,
        useSharedStrings: true
    };

    var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    workbook.zip.pipe(res);
    var worksheet_opch = workbook.addWorksheet("OPCH");
    var worksheet_pch1 = workbook.addWorksheet("PCH1");

    var CreditHeader = {
        DocNum: "DocNum",
        DocType: "DocType",
        DocDate: "DocDate",
        DocDueDate: "DocDueDate",
        CardCode: "CardCode",
        Address: "Address",
        NumAtCard: "NumAtCard",
        Comments: "Comments",
        JournalMemo: "JournalMemo",
        PaymentGroupCode: "PaymentGroupCode",
        SalesPersonCode: "SalesPersonCode",
        TaxDate: "TaxDate",
        Address2: "Address2",
        ControlAccount: "ControlAccount",
        U_OrigEntry: "U_OrigEntry",
        U_OrigNum: "U_OrigNum"
    };

    worksheet_opch.columns = [{
        header: CreditHeader.DocNum,
        key: CreditHeader.DocNum,
        width: columnWidth
    }, {
        header: CreditHeader.DocType,
        key: CreditHeader.DocType,
        width: columnWidth
    }, {
        header: CreditHeader.DocDate,
        key: CreditHeader.DocDate,
        width: columnWidth
    }, {
        header: CreditHeader.DocDueDate,
        key: CreditHeader.DocDueDate,
        width: columnWidth
    }, {
        header: CreditHeader.CardCode,
        key: CreditHeader.CardCode,
        width: columnWidth
    }, {
        header: CreditHeader.Address,
        key: CreditHeader.Address,
        width: 60
    }, {
        header: CreditHeader.NumAtCard,
        key: CreditHeader.NumAtCard,
        width: columnWidth
    }, {
        header: CreditHeader.Comments,
        key: CreditHeader.Comments,
        width: columnWidth
    }, {
        header: CreditHeader.JournalMemo,
        key: CreditHeader.JournalMemo,
        width: columnWidth
    }, {
        header: CreditHeader.PaymentGroupCode,
        key: CreditHeader.PaymentGroupCode,
        width: columnWidth
    }, {
        header: CreditHeader.SalesPersonCode,
        key: CreditHeader.SalesPersonCode,
        width: columnWidth
    }, {
        header: CreditHeader.TaxDate,
        key: CreditHeader.TaxDate,
        width: columnWidth
    }, {
        header: CreditHeader.Address2,
        key: CreditHeader.Address2,
        width: 60
    }, {
        header: CreditHeader.ControlAccount,
        key: CreditHeader.ControlAccount,
        width: columnWidth
    }, {
        header: CreditHeader.U_OrigEntry,
        key: CreditHeader.U_OrigEntry,
        width: columnWidth
    }, {
        header: CreditHeader.U_OrigNum,
        key: CreditHeader.U_OrigNum,
        width: columnWidth
    }];

    var CreditItem = {
        ParentKey: "ParentKey",
        LineNum: "LineNum",
        ItemCode: "ItemCode",
        Quantity: "Quantity",
        Price: "Price",
        DiscountPercent: "DiscountPercent",
        WarehouseCode: "WarehouseCode",
        TaxCode: "TaxCode",
        WTLiable: "WTLiable",
        LineTotal: "LineTotal",
        UnitPrice: "UnitPrice",
        U_OrigEntry: "U_OrigEntry",
        U_OrigLine: "U_OrigLine",
        Description: "Description",
        Balance: "Balance"
    };

    worksheet_pch1.columns = [{
        header: CreditItem.ParentKey,
        key: CreditItem.ParentKey,
        width: columnWidth
    }, {
        header: CreditItem.LineNum,
        key: CreditItem.LineNum,
        width: columnWidth
    }, {
        header: CreditItem.ItemCode,
        key: CreditItem.ItemCode,
        width: columnWidth
    }, {
        header: CreditItem.Quantity,
        key: CreditItem.Quantity,
        width: columnWidth
    }, {
        header: CreditItem.Price,
        key: CreditItem.Price,
        width: columnWidth
    }, {
        header: CreditItem.DiscountPercent,
        key: CreditItem.DiscountPercent,
        width: columnWidth
    }, {
        header: CreditItem.WarehouseCode,
        key: CreditItem.WarehouseCode,
        width: columnWidth
    }, {
        header: CreditItem.TaxCode,
        key: CreditItem.TaxCode,
        width: columnWidth
    }, {
        header: CreditItem.WTLiable,
        key: CreditItem.WTLiable,
        width: columnWidth
    }, {
        header: CreditItem.LineTotal,
        key: CreditItem.LineTotal,
        width: columnWidth
    }, {
        header: CreditItem.UnitPrice,
        key: CreditItem.UnitPrice,
        width: columnWidth
    }, {
        header: CreditItem.U_OrigEntry,
        key: CreditItem.U_OrigEntry,
        width: columnWidth
    }, {
        header: CreditItem.U_OrigLine,
        key: CreditItem.U_OrigLine,
        width: columnWidth
    }, {
        header: CreditItem.Description,
        key: CreditItem.Description,
        width: columnWidth
    }, {
        header: CreditItem.Balance,
        key: CreditItem.Balance,
        width: columnWidth
    }];
    // Get Term APIs For DocDue Date

    qbo.findCreditMemos([
        {field: 'TxnDate', value: '2017-08-01', operator: '>='},
        {field: 'TxnDate', value: '2017-09-30', operator: '<='}, 
        {field: 'limit',value: 900}], function(err, credit) {
        jsonObj = credit;

        var key = 'CreditMemo';
        credits = jsonObj.QueryResponse[key];
        var docnum = 1000
        for (var i = 0; i < credits.length; i++) {
            docnum = docnum + 1;
            //var isAddress = !Object.keys(credits[i].BillAddr.Line6).length
            if (credits[i].hasOwnProperty("BillAddr")) {
                var Billaddrs = credits[i].BillAddr.Line1
                if (credits[i].BillAddr.hasOwnProperty("Line2")) {
                    //console.log("It is working")
                    Billaddrs += credits[i].BillAddr.Line2 + " "
                }
                if (credits[i].BillAddr.hasOwnProperty("Line3")) {
                    // console.log("It is working")
                    Billaddrs += credits[i].BillAddr.Line3 + " "
                }
                if (credits[i].BillAddr.hasOwnProperty("Line4")) {
                    //console.log("It is working")
                    Billaddrs += credits[i].BillAddr.Line4 + " "
                }
                if (credits[i].BillAddr.hasOwnProperty("City")) {
                    Billaddrs += credits[i].BillAddr.City + " "
                }
                if (credits[i].BillAddr.hasOwnProperty("PostalCode")) {
                    Billaddrs += credits[i].BillAddr.PostalCode + " "
                }
                if (credits[i].BillAddr.hasOwnProperty("CountrySubDivisionCode")) {
                    Billaddrs += credits[i].BillAddr.CountrySubDivisionCode
                }

            } else {
                Billaddrs = '';
            }

            var comments = '';
            if (credits[i].hasOwnProperty("PrivateNote")) {
                comments = credits[i].PrivateNote
            }            

            worksheet_opch.addRow({
                DocNum: docnum,
                DocType: 'dDocument_Items',
                DocDate: credits[i].TxnDate,
                DocDueDate: '',
                CardCode: credits[i].CustomerRef.name,
                Address: Billaddrs,
                NumAtCard: credits[i].DocNumber,
                Comments: comments,
                JournalMemo: 'Credit Memo',
                PaymentGroupCode: '',
                SalesPersonCode: '',
                TaxDate: credits[i].TxnDate,
                Address2: '',
                ControlAccount: '',
                U_OrigEntry: credits[i].Id,
                U_OrigNum: credits[i].DocNumber
            }).commit;

            var Line_key = 'Line';
            var Lines = credits[i][Line_key];
            var Balance = " ";
            if (credits[i].hasOwnProperty("Balance")) {
                var Balance = credits[i].Balance;
            } 

            for (var j = 0; j < Lines.length - 1; j++) {
                var Description = "";
                if (Lines[j].hasOwnProperty("Description")) {
                    Description = Lines[j].Description;
                } 
             // console.log(docnum,Billaddrs,comments,credits[i].CustomerRef.name,credits[i].DocNumber,credits[i].Id,credits[i].TxnDate, Balance, Description);
               //onsole.log(docnumber);
                // if(Lines[j].hasOwnProperty("DetailType") && Lines[j]["DetailType"]){
                var detailType = Lines[j]["DetailType"];
                if (detailType == "SalesItemLineDetail") {

                    var salesItem = Lines[j]["SalesItemLineDetail"]
                    worksheet_pch1.addRow({
                        ParentKey: docnum,
                        LineNum: '',
                        ItemCode: salesItem.ItemRef.name,
                        Quantity: Lines[j]["SalesItemLineDetail"].Qty,
                        Price: Lines[j]["SalesItemLineDetail"].UnitPrice,
                        DiscountPercent: '0',
                        WarehouseCode: '',
                        TaxCode: '',
                        WTLiable: 'tNo',
                        LineTotal: Lines[j].Amount,
                        UnitPrice: Lines[j]["SalesItemLineDetail"].UnitPrice,
                        U_OrigEntry: credits[i].Id,
                        U_OrigLine: Lines[j].LineNum,
                        Description: Description,
                        Balance: Balance
                    }).commit;
                }
            };
        };
        worksheet_opch.commit();
        worksheet_pch1.commit();
        workbook.commit();
    });
});

module.exports = router;