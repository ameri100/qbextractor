var express = require('express'),
    request = require('request'),
    cookieSession = require('cookie-session'),
    http = require('http'),
    port = process.env.PORT || 3000,
    request = require('request'),
    qs = require('querystring'),
    util = require('util');
var path = require('path');
var router = express.Router(),
    QuickBooks = require('../../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = ''
    jsonObj = {
        'key': 'value'
    },
    jsonObjItem = {
        'key': 'value'
    },
    columnWidth = 30;
var timeCharge = {
        "": ""
    };

// putting all invoice data into excel file (Invoice Header) OPCH
router.post("/getTime", function(req, res) {

    var options = {
        useStyles: true,
        useSharedStrings: true
    };

    var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    workbook.zip.pipe(res);
    var worksheet = workbook.addWorksheet("My Sheet");

    var timeHeader = {
        DocNum: "DocNum",
        DocType: "DocType",
        DocDate: "DocDate",
        DocDueDate: "DocDueDate",
        CardCode: "CardCode",
        Address: "Address",
        NumAtCard: "NumAtCard",
        Comments: "Comments",
        JournalMemo: "JournalMemo",
        PaymentGroupCode: "PaymentGroupCode",
        SalesPersonCode: "SalesPersonCode",
        TaxDate: "TaxDate",
        Address2: "Address2",
        ControlAccount: "ControlAccount",
        U_OrigEntry: "U_OrigEntry",
        U_OrigNum: "U_OrigNum",
    };

    worksheet.columns = [{
        header: timeHeader.DocNum,
        key: timeHeader.DocNum,
        width: columnWidth
    }, {
        header: timeHeader.DocType,
        key: timeHeader.DocType,
        width: columnWidth
    }, {
        header: timeHeader.DocDate,
        key: timeHeader.DocDate,
        width: columnWidth
    }, {
        header: timeHeader.DocDueDate,
        key: timeHeader.DocDueDate,
        width: columnWidth
    }, {
        header: timeHeader.CardCode,
        key: timeHeader.CardCode,
        width: columnWidth
    }, {
        header: timeHeader.Address,
        key: timeHeader.Address,
        width: 60
    }, {
        header: timeHeader.NumAtCard,
        key: timeHeader.NumAtCard,
        width: columnWidth
    }, {
        header: timeHeader.Comments,
        key: timeHeader.Comments,
        width: columnWidth
    }, {
        header: timeHeader.JournalMemo,
        key: timeHeader.JournalMemo,
        width: columnWidth
    }, {
        header: timeHeader.PaymentGroupCode,
        key: timeHeader.PaymentGroupCode,
        width: columnWidth
    }, {
        header: timeHeader.SalesPersonCode,
        key: timeHeader.SalesPersonCode,
        width: columnWidth
    }, {
        header: timeHeader.TaxDate,
        key: timeHeader.TaxDate,
        width: columnWidth
    }, {
        header: timeHeader.Address2,
        key: timeHeader.Address2,
        width: 60
    }, {
        header: timeHeader.ControlAccount,
        key: timeHeader.ControlAccount,
        width: columnWidth
    }, {
        header: timeHeader.U_OrigEntry,
        key: timeHeader.U_OrigEntry,
        width: columnWidth
    }, {
        header: timeHeader.U_OrigNum,
        key: timeHeader.U_OrigNum,
        width: columnWidth
    }];
     
    qbo.findTimeActivities([
      {field: 'MetaData.CreateTime', value: '2016-01-01T04:05:05-07:00', operator: '>='},
      {field: 'MetaData.CreateTime', value: '2016-06-15T04:05:05-07:00', operator: '<='},
      {field: 'limit', value: 900}
      ], function(err, credit) {
        jsonObj = credit;

        var key = 'TimeActivity';
        timeCharge = jsonObj.QueryResponse[key];
        
        var docnum = 1000
        for (var i = 0; i < timeCharge.length; i++) {
            docnum = docnum + 1;
             // console.log("comments = ",comments);
                worksheet.addRow({
                    DocNum: docnum,
                    DocType: 'dDocument_Items',
                    DocDate: timeCharge[i].TxnDate,
                    DocDueDate: '',
                    CardCode: timeCharge[i].CustomerRef.name,
                    Address: '',
                    NumAtCard: timeCharge[i].Id,
                    Comments: timeCharge[i].Description,
                    JournalMemo: 'Time Charge',
                    PaymentGroupCode: '',
                    SalesPersonCode: '',
                    TaxDate: timeCharge[i].TxnDate,
                    Address2: '',
                    ControlAccount: '',
                    U_OrigEntry: timeCharge[i].Id,   
                    U_OrigNum: timeCharge[i].Id  
                }).commit;
        };
        worksheet.commit();
        workbook.commit();
    });
});

router.post("/getTimeItem", function(req, res) {

    var options = {
        useStyles: true,
        useSharedStrings: true
    };

    var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    workbook.zip.pipe(res);
    var worksheet = workbook.addWorksheet("My Sheet");

    var CreditItem = {
        ParentKey: "ParentKey",
        LineNum: "LineNum",
        ItemCode: "ItemCode",
        Quantity: "Quantity",
        Price: "Price",
        DiscountPercent: "DiscountPercent",
        WarehouseCode: "WarehouseCode",
        TaxCode: "TaxCode",
        WTLiable: "WTLiable",
        LineTotal: "LineTotal",
        UnitPrice: "UnitPrice",
        U_OrigEntry: "U_OrigEntry",
        U_OrigLine: "U_OrigLine",
        Description: "Description",
        Balance: "Balance"
    };

    worksheet.columns = [{
            header: CreditItem.ParentKey,
            key: CreditItem.ParentKey,
            width: columnWidth
        }, {
            header: CreditItem.LineNum,
            key: CreditItem.LineNum,
            width: columnWidth
        }, {
            header: CreditItem.ItemCode,
            key: CreditItem.ItemCode,
            width: columnWidth
        }, {
            header: CreditItem.Quantity,
            key: CreditItem.Quantity,
            width: columnWidth
        }, {
            header: CreditItem.Price,
            key: CreditItem.Price,
            width: columnWidth
        }, {
            header: CreditItem.DiscountPercent,
            key: CreditItem.DiscountPercent,
            width: columnWidth
        }, {
            header: CreditItem.WarehouseCode,
            key: CreditItem.WarehouseCode,
            width: columnWidth
        }, {
            header: CreditItem.TaxCode,
            key: CreditItem.TaxCode,
            width: columnWidth
        }, {
            header: CreditItem.WTLiable,
            key: CreditItem.WTLiable,
            width: columnWidth
        }, {
            header: CreditItem.LineTotal,
            key: CreditItem.LineTotal,
            width: columnWidth
        }, {
            header: CreditItem.UnitPrice,
            key: CreditItem.UnitPrice,
            width: columnWidth
        }, {
            header: CreditItem.U_OrigEntry,
            key: CreditItem.U_OrigEntry,
            width: columnWidth
        }, {
            header: CreditItem.U_OrigLine,
            key: CreditItem.U_OrigLine,
            width: columnWidth
        }
    ];

    //var key = 'CredirtMemo';
    qbo.findTimeActivities([
      {field: 'MetaData.CreateTime', value: '2016-01-01T04:05:05-07:00', operator: '>='},
      {field: 'MetaData.CreateTime', value: '2016-06-15T04:05:05-07:00', operator: '<='},
      {field: 'limit', value: 900}
      ], function(err, credit) {

        jsonObjItem = credit;

        timeCharge = jsonObjItem.QueryResponse.TimeActivity;
        var docnumber = 1000
        for (var i = 0; i < timeCharge.length; i++) {
            docnumber = docnumber + 1;  
                var Qty = timeCharge[i].Hours +':'+ timeCharge[i].Minutes;

                console.log("Item Code = ",timeCharge[i].EmployeeRef.name);
                console.log("Qty = ",Qty);
                console.log("Price and UnitPrice = ",timeCharge[i].HourlyRate);
                console.log("U_OrigEntry = ",timeCharge[i].Id);


                    worksheet.addRow({
                            ParentKey: docnumber,
                            LineNum: '',
                            ItemCode: timeCharge[i].EmployeeRef.name,
                            Quantity: Qty,
                            Price: timeCharge[i].HourlyRate,
                            DiscountPercent: '0',
                            WarehouseCode: '',
                            TaxCode: '',
                            WTLiable: 'tNo',
                            LineTotal: '',
                            UnitPrice: timeCharge[i].HourlyRate,
                            U_OrigEntry: timeCharge[i].Id,
                            U_OrigLine: ""
                        }).commit;
        };
        worksheet.commit();
        workbook.commit();
    });
});
module.exports = router;