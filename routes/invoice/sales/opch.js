var express = require('express'),
    request = require('request'),
    cookieSession = require('cookie-session'),
    http = require('http'),
    port = process.env.PORT || 3000,
    request = require('request'),
    qs = require('querystring'),
    util = require('util');
var path = require('path');
var router = express.Router(),
    QuickBooks = require('../../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
    tmpEmployeeInfo = {},
    jsonObj = {},
    jsonObjTerm = {},
    columnWidth = 30;
var incomingRecordCount = 500
var invoices = {},
    terms = {};
var k = 0;
//var invoices = new Array();
var jsonObjArray = new Array();
var noDuplicateArray = new Array();
var uniqeJsonArray = new Array();
// Render invoices page and get all Invoice Data
router.get('/', function(req, res) {

    qbo.findTerms('', function(err, term) {
        if (err)
            return res.status(400).json(err);
        var obj = {};
        jsonObjTerm = term;
        res.render('invoices.ejs', {
            locals: {
                invoice: term
            }
        });
    });
});

function FetchObjects(totalCount, value, loopNo) {
    //  setTimeout(function(totalCount) {
    var totalCount = totalCount.toString();
    var value = value.toString();
    var startDate = "\'2017-08-01\'";
    var endDate = "\'2017-09-30\'"
    var query = "WHERE TxnDate >=" + startDate + "AND TxnDate <= " + endDate + " MAXRESULTS " + totalCount + " startPosition " + value;
    qbo.findInvoices(query, function(err, invoice) {
        console.log("After function call = ", invoice, invoice.QueryResponse["totalCount"]);

        if (isEmptyObject(invoice)) {
            return true
        };
        console.log("Respone is not coming ", k);
        jsonObjArray.push(invoice.QueryResponse['Invoice']);
        k++;
    });
}


function ArrNoDupe(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    return Object.keys(temp);
}

function isEmptyObject(obj) {
    for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            return false;
        }
    }
    return true;
}

function uniqueArrayFunction() {
    console.log("Count in data 12", jsonObjArray.length);
    noDuplicateArray.length = 0
    for (var z = 0; z < jsonObjArray.length; z++) {
        invoices = JSON.parse(JSON.stringify(jsonObjArray[z]));
        console.log("Count in data 123", invoices.length);
        for (var i = 0; i < invoices.length; i++) {

            noDuplicateArray.push(invoices[i])

        };
    };

    console.log("Count in data 1212", noDuplicateArray.length);

    var count1 = 0
    for (var z = 0; z < noDuplicateArray.length; z++) {

        if (uniqeJsonArray.length > 0) {
            var isEqual = 0
            for (var i = 0; i < uniqeJsonArray.length; i++) {

                if (uniqeJsonArray[i].Id == noDuplicateArray[z].Id) {
                    isEqual = 1
                    count1++;
                    console.log("Count in data", count1);
                };
            };
            if (isEqual == 0) {
                uniqeJsonArray.push(noDuplicateArray[z])
            };
        } else {
            uniqeJsonArray.push(noDuplicateArray[z])
        }
    };
}


router.post("/callObjects", function(req, res) {


    FetchObjects(incomingRecordCount, (incomingRecordCount * k), (1 * k))

});


// putting all invoice data into excel file (Invoice Header) OPCH
router.post("/getInvoice", function(req, res) {

    var options = {
        useStyles: true,
        useSharedStrings: true
    };

    var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    workbook.zip.pipe(res);
    var worksheet_opch = workbook.addWorksheet("OPCH");
    var worksheet_pch1 = workbook.addWorksheet("PCH1");

    var invoiceHeader = {
        DocNum: "DocNum",
        DocType: "DocType",
        DocDate: "DocDate",
        DocDueDate: "DocDueDate",
        CardCode: "CardCode",
        Address: "Address",
        NumAtCard: "NumAtCard",
        Comments: "Comments",
        JournalMemo: "JournalMemo",
        PaymentGroupCode: "PaymentGroupCode",
        SalesPersonCode: "SalesPersonCode",
        TaxDate: "TaxDate",
        Address2: "Address2",
        ControlAccount: "ControlAccount",
        U_OrigEntry: "U_OrigEntry",
        U_OrigNum: "U_OrigNum"
    };

    worksheet_opch.columns = [{
        header: invoiceHeader.DocNum,
        key: invoiceHeader.DocNum,
        width: columnWidth
    }, {
        header: invoiceHeader.DocType,
        key: invoiceHeader.DocType,
        width: columnWidth
    }, {
        header: invoiceHeader.DocDate,
        key: invoiceHeader.DocDate,
        width: columnWidth
    }, {
        header: invoiceHeader.DocDueDate,
        key: invoiceHeader.DocDueDate,
        width: columnWidth
    }, {
        header: invoiceHeader.CardCode,
        key: invoiceHeader.CardCode,
        width: columnWidth
    }, {
        header: invoiceHeader.Address,
        key: invoiceHeader.Address,
        width: 60
    }, {
        header: invoiceHeader.NumAtCard,
        key: invoiceHeader.NumAtCard,
        width: columnWidth
    }, {
        header: invoiceHeader.Comments,
        key: invoiceHeader.Comments,
        width: columnWidth
    }, {
        header: invoiceHeader.JournalMemo,
        key: invoiceHeader.JournalMemo,
        width: columnWidth
    }, {
        header: invoiceHeader.PaymentGroupCode,
        key: invoiceHeader.PaymentGroupCode,
        width: columnWidth
    }, {
        header: invoiceHeader.SalesPersonCode,
        key: invoiceHeader.SalesPersonCode,
        width: columnWidth
    }, {
        header: invoiceHeader.TaxDate,
        key: invoiceHeader.TaxDate,
        width: columnWidth
    }, {
        header: invoiceHeader.Address2,
        key: invoiceHeader.Address2,
        width: 60
    }, {
        header: invoiceHeader.ControlAccount,
        key: invoiceHeader.ControlAccount,
        width: columnWidth
    }, {
        header: invoiceHeader.U_OrigEntry,
        key: invoiceHeader.U_OrigEntry,
        width: columnWidth
    }, {
        header: invoiceHeader.U_OrigNum,
        key: invoiceHeader.U_OrigNum,
        width: columnWidth
    }];

    var salesInvoiceItem = {
        ParentKey: "ParentKey",
        LineNum: "LineNum",
        ItemCode: "ItemCode",
        Quantity: "Quantity",
        Price: "Price",
        DiscountPercent: "DiscountPercent",
        WarehouseCode: "WarehouseCode",
        TaxCode: "TaxCode",
        WTLiable: "WTLiable",
        LineTotal: "LineTotal",
        UnitPrice: "UnitPrice",
        U_OrigEntry: "U_OrigEntry",
        U_OrigLine: "U_OrigLine",
        Description: "Description"
    };

    worksheet_pch1.columns = [{
        header: salesInvoiceItem.ParentKey,
        key: salesInvoiceItem.ParentKey,
        width: columnWidth
    }, {
        header: salesInvoiceItem.LineNum,
        key: salesInvoiceItem.LineNum,
        width: columnWidth
    }, {
        header: salesInvoiceItem.ItemCode,
        key: salesInvoiceItem.ItemCode,
        width: columnWidth
    }, {
        header: salesInvoiceItem.Quantity,
        key: salesInvoiceItem.Quantity,
        width: columnWidth
    }, {
        header: salesInvoiceItem.Price,
        key: salesInvoiceItem.Price,
        width: columnWidth
    }, {
        header: salesInvoiceItem.DiscountPercent,
        key: salesInvoiceItem.DiscountPercent,
        width: columnWidth
    }, {
        header: salesInvoiceItem.WarehouseCode,
        key: salesInvoiceItem.WarehouseCode,
        width: columnWidth
    }, {
        header: salesInvoiceItem.TaxCode,
        key: salesInvoiceItem.TaxCode,
        width: columnWidth
    }, {
        header: salesInvoiceItem.WTLiable,
        key: salesInvoiceItem.WTLiable,
        width: columnWidth
    }, {
        header: salesInvoiceItem.LineTotal,
        key: salesInvoiceItem.LineTotal,
        width: columnWidth
    }, {
        header: salesInvoiceItem.UnitPrice,
        key: salesInvoiceItem.UnitPrice,
        width: columnWidth
    }, {
        header: salesInvoiceItem.U_OrigEntry,
        key: salesInvoiceItem.U_OrigEntry,
        width: columnWidth
    }, {
        header: salesInvoiceItem.U_OrigLine,
        key: salesInvoiceItem.U_OrigLine,
        width: columnWidth
    }, {
        header: salesInvoiceItem.Description,
        key: salesInvoiceItem.Description,
        width: columnWidth
    }];

    // Get Term APIs For DocDue Date
    uniqueArrayFunction();
    //var key = 'Invoice';
    //invoices = jsonObj.QueryResponse[key];
    console.log('Length of array ', noDuplicateArray.length)

    var docnum = 1000;

    // for (var z = 0; z < jsonObjArray.length; z++) {
    //     invoices = JSON.parse(JSON.stringify(jsonObjArray[z]));
    //     console.log("Count Z ",z)
    invoices = JSON.parse(JSON.stringify(uniqeJsonArray));
    //invoices = JSON.parse(JSON.stringify(noDuplicateArray));
    for (var i = 0; i < invoices.length; i++) {
        // invoices = JSON.parse(JSON.stringify(uniqeJsonArray[i]));
        //console.log("Count i ",i)
        docnum = docnum + 1;
        var tmpNumAtCard = '';
        if (invoices[i].hasOwnProperty("CustomField")) {
            console.log("it is coming1 1")
            if (invoices[i].CustomField[0].DefinitionId === "1") {
                console.log("it is coming 2")
                if (invoices[i].CustomField[0].hasOwnProperty("StringValue")) {
                    console.log("it is coming 12")
                    tmpNumAtCard = invoices[i].DocNumber + "#" + invoices[i].CustomField[0].StringValue;
                } else {
                    tmpNumAtCard = invoices[i].DocNumber;
                }
            } else {
                tmpNumAtCard = invoices[i].DocNumber;
            }
        } else {
            tmpNumAtCard = invoices[i].DocNumber;
        }

        var Billaddrs = '';
            if (invoices[i].hasOwnProperty("BillAddr")) {
               // var Billaddrs = invoices[i].BillAddr.Line1
                if (invoices[i].BillAddr.hasOwnProperty("Line1")) {
                    //console.log("It is working")
                    Billaddrs += invoices[i].BillAddr.Line1 + " "
                }
                if (invoices[i].BillAddr.hasOwnProperty("Line2")) {
                    //console.log("It is working")
                    Billaddrs += invoices[i].BillAddr.Line2 + " "
                }
                if (invoices[i].BillAddr.hasOwnProperty("Line3")) {
                    // console.log("It is working")
                    Billaddrs += invoices[i].BillAddr.Line3 + " "
                }
                if (invoices[i].BillAddr.hasOwnProperty("Line4")) {
                    //console.log("It is working")
                    Billaddrs += invoices[i].BillAddr.Line4 + " "
                }
                if (invoices[i].BillAddr.hasOwnProperty("Line5")) {
                    //console.log("It is working")
                    Billaddrs += invoices[i].BillAddr.Line5 + " "
                }
                if (invoices[i].BillAddr.hasOwnProperty("City")) {
                    Billaddrs += invoices[i].BillAddr.City + " "
                }
                if (invoices[i].BillAddr.hasOwnProperty("CountrySubDivisionCode")) {
                    Billaddrs += invoices[i].BillAddr.CountrySubDivisionCode + " "
                }
                if (invoices[i].BillAddr.hasOwnProperty("PostalCode")) {
                    Billaddrs += invoices[i].BillAddr.PostalCode 
                }
            }
        //console.log(Billaddrs);
        var Shipaddrs = ''
            if (invoices[i].hasOwnProperty("ShipAddr")) {
                if (invoices[i].ShipAddr.hasOwnProperty("Line1")) {
                    //console.log("It is working")
                    Shipaddrs += invoices[i].ShipAddr.Line1 + " "
                }               
                if (invoices[i].ShipAddr.hasOwnProperty("Line2")) {
                    //console.log("It is working")
                    Shipaddrs += invoices[i].ShipAddr.Line2 + " "
                }
                if (invoices[i].ShipAddr.hasOwnProperty("Line3")) {
                    // console.log("It is working")
                    Shipaddrs += invoices[i].ShipAddr.Line3 + " "
                }
                if (invoices[i].ShipAddr.hasOwnProperty("Line4")) {
                    //console.log("It is working")
                    Shipaddrs += invoices[i].ShipAddr.Line4 + " "
                }
                if (invoices[i].ShipAddr.hasOwnProperty("Line5")) {
                    //console.log("It is working")
                    Shipaddrs += invoices[i].ShipAddr.Line5 + " "
                }
                if (invoices[i].ShipAddr.hasOwnProperty("City")) {
                    Shipaddrs += invoices[i].ShipAddr.City + " "
                }
                if (invoices[i].ShipAddr.hasOwnProperty("CountrySubDivisionCode")) {
                    Shipaddrs += invoices[i].ShipAddr.CountrySubDivisionCode + " "
                }
                if (invoices[i].ShipAddr.hasOwnProperty("PostalCode")) {
                    Shipaddrs += invoices[i].ShipAddr.PostalCode                
                }
            }
            

        terms = jsonObjTerm.QueryResponse["Term"];

        var paymentGroupCode = ''
        var tmpDocDueDays = new Date();

        var DocDueDays = "";
        if (invoices[i].hasOwnProperty("DueDate") && invoices[i].DueDate != "") {
            DocDueDays = invoices[i].DueDate;
            for (var t = terms.length - 1; t >= 0; t--) {
                if (terms[t].Id == invoices[i].SalesTermRef.value) {
                    paymentGroupCode = terms[t].Name;
                    worksheet_opch.addRow({
                        // JournalMemo data not found in QuickBooks
                        DocNum: docnum,
                        DocType: 'dDocument_Items',
                        DocDate: invoices[i].TxnDate,
                        // we need to check data if we are getting from BellSoft

                        DocDueDate: DocDueDays,
                        CardCode: invoices[i].CustomerRef.name,
                        // Bill Address
                        Address: Billaddrs,
                        // same as U_OriginNum + "#" + P.O. Number
                        NumAtCard: tmpNumAtCard,

                        Comments: invoices[i].PrivateNote,
                        JournalMemo: 'Opening Balance',
                        // check in company API
                        PaymentGroupCode: paymentGroupCode,
                        SalesPersonCode: '',
                        TaxDate: invoices[i].TxnDate,
                        // Ship Address
                        Address2: Shipaddrs,

                        ControlAccount: '',
                        U_OrigEntry: invoices[i].Id,
                        U_OrigNum: invoices[i].DocNumber

                    }).commit;
                }
            }
        } else {
            for (var t = terms.length - 1; t >= 0; t--) {
                if (terms[t].Id == invoices[i].SalesTermRef.value) {
                    var days = terms[t].DueDays;
                    var tmpdocDate = new Date(invoices[i].TxnDate);
                    paymentGroupCode = terms[t].Name;

                    if (terms[t].hasOwnProperty("DueDays")) {
                        tmpdocDate.setDate(tmpdocDate.getDate() + days);

                        tmpDocDueDays = tmpdocDate.toISOString().slice(0, 10);
                        worksheet_opch.addRow({
                            // JournalMemo data not found in QuickBooks
                            DocNum: docnum,
                            DocType: 'dDocument_Items',
                            DocDate: invoices[i].TxnDate,
                            // we need to check data if we are getting from BellSoft

                            DocDueDate: tmpDocDueDays,
                            CardCode: invoices[i].CustomerRef.name,
                            // Bill Address
                            Address: Billaddrs,
                            // same as U_OriginNum + "#" + P.O. Number
                            NumAtCard: tmpNumAtCard,

                            Comments: invoices[i].CustomerMemo.value,
                            JournalMemo: 'Opening Balance',
                            // check in company API
                            PaymentGroupCode: paymentGroupCode,
                            SalesPersonCode: '',
                            TaxDate: invoices[i].TxnDate,
                            // Ship Address
                            Address2: Shipaddrs,

                            ControlAccount: '',
                            U_OrigEntry: invoices[i].Id,
                            U_OrigNum: invoices[i].DocNumber

                        }).commit;
                    }
                }
            }
        }
        var Line_key = 'Line';
        var Lines = invoices[i][Line_key];
        // console.log('Length of Line Item ',Lines.length);
        for (var j = 0; j < Lines.length - 1; j++) {
             var itemCode = '',
                Description = "",
                amount = "",
                quantity = '',
                UnitPrice = '';
            //Description if exists
            if (Lines[j].hasOwnProperty("Description")) {
                var Description = Lines[j].Description;
            }
            //Amount if Exist
            if (Lines[j].hasOwnProperty("Amount")) {
                var amount = Lines[j].Amount;
            } 
            var detailType = Lines[j]["DetailType"];
            if (detailType.toString() === "SalesItemLineDetail") {
                //ItemCode if Exists
                if (Lines[j]["SalesItemLineDetail"].hasOwnProperty("ItemRef")) {
                    var itemCode = Lines[j]["SalesItemLineDetail"].ItemRef.name
                } 
                //Quantity if Exists
                if (Lines[j]["SalesItemLineDetail"].hasOwnProperty("Qty")) {
                    //var quantity = Lines[j]["SalesItemLineDetail"].Qty;
                    var quantity = (Lines[j].Amount / Lines[j]["SalesItemLineDetail"].UnitPrice)
                }
                //Unit Price If Exists
                if (Lines[j]["SalesItemLineDetail"].hasOwnProperty("UnitPrice")) {
                    var UnitPrice = Lines[j]["SalesItemLineDetail"].UnitPrice;
                }
            } else if (detailType.toString() === "SubTotalLineDetail") {
                var itemCode = '';
                var quantity = '';
                var UnitPrice = '';
            }
            worksheet_pch1.addRow({
                ParentKey: docnum,
                LineNum: '',
                ItemCode: itemCode,
                Quantity: quantity,
                Price: UnitPrice,
                DiscountPercent: '0',
                WarehouseCode: '',
                TaxCode: '',
                WTLiable: 'tNo',
                LineTotal: amount,
                UnitPrice: UnitPrice,
                U_OrigEntry: invoices[i].Id,
                U_OrigLine: Lines[j].LineNum,
                Description: Description
            }).commit;
        }
    };
    // };
    console.log('Length of invoices ', invoices.length, invoices)

    worksheet_opch.commit();
    worksheet_pch1.commit();
    workbook.commit();
});

module.exports = router;