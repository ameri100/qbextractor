var express = require('express'),
    request = require('request'),
    cookieSession = require('cookie-session'),
    http = require('http'),
    port = process.env.PORT || 3000,
    request = require('request'),
    qs = require('querystring'),
    util = require('util');
var path = require('path');
var router = express.Router(),
    QuickBooks = require('../../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
    tmpEmployeeInfo = {
        'key': 'value'
    },
    jsonObj = {
        'key': 'value'
    },
    jsonObjTerm = {
        'key': 'value'
    },
    columnWidth = 30;
var invoices = {
        "": ""
    },
    terms = {
        "": ""
    };
// Render invoices page and get all Invoice Data
router.get('/', function(req, res) {

    qbo.findTerms('', function(err, term) {
        var obj = {
            'key': 'value'
        };
        jsonObjTerm = term;
    });

    // Getting Invoice from QuickBooks
        qbo.findInvoices("",function(err, invoice) {          
          
            jsonObj = invoice;
            res.render('invoices.ejs', {
                locals: {
                    invoice: invoice
                }
                });                 
        });

     /*   FetchObjects(totalCount);
    function FetchObjects(totalCount){[
          {field: 'MetaData.CreateTime', value: '2016-01-01T04:05:05-07:00', operator: '>='},
          {field: 'MetaData.CreateTime', value: '2016-06-17T04:05:05-07:00', operator: '<='},
          {field: 'limit', value: 750},
          {field: 'offset', value: totalCount}
          ], */
          // write here 
           /* jsonObj.push(invoice.QueryResponse);
            console.log("first line inside Function = ", totalCount);   
            //add up the count of objects  
            totalCount += invoice.QueryResponse["totalCount"];
            // check if the object crosses the total length
            if(totalCount == 1563){
                console.log("end of loop");
                res.render('invoices.ejs', {
                locals: {
                    invoice: invoice
                }
                });
                return false;
            } 
            //if object exists
            if(invoice.QueryResponse.Invoice){
                invoice = '';
                console.log("Hi ! looping is in progress")
                //loop through the objects and store
                FetchObjects(totalCount);
                //k++;
            } }
        */
});

// putting all invoice data into excel file (Invoice Header) OPCH
router.post("/getInvoice", function(req, res) {

    var options = {
        useStyles: true,
        useSharedStrings: true
    };

    var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    workbook.zip.pipe(res);
    var worksheet = workbook.addWorksheet("My Sheet");

    var invoiceHeader = {
        DocNum: "DocNum",
        DocType: "DocType",
        DocDate: "DocDate",
        DocDueDate: "DocDueDate",
        CardCode: "CardCode",
        Address: "Address",
        NumAtCard: "NumAtCard",
        Comments: "Comments",
        JournalMemo: "JournalMemo",
        PaymentGroupCode: "PaymentGroupCode",
        SalesPersonCode: "SalesPersonCode",
        TaxDate: "TaxDate",
        Address2: "Address2",
        ControlAccount: "ControlAccount",
        U_OrigEntry: "U_OrigEntry",
        U_OrigNum: "U_OrigNum"
    };

    worksheet.columns = [{
            header: invoiceHeader.DocNum,
            key: invoiceHeader.DocNum,
            width: columnWidth
        }, {
            header: invoiceHeader.DocType,
            key: invoiceHeader.DocType,
            width: columnWidth
        }, {
            header: invoiceHeader.DocDate,
            key: invoiceHeader.DocDate,
            width: columnWidth
        }, {
            header: invoiceHeader.DocDueDate,
            key: invoiceHeader.DocDueDate,
            width: columnWidth
        }, {
            header: invoiceHeader.CardCode,
            key: invoiceHeader.CardCode,
            width: columnWidth
        },
        {
            header: invoiceHeader.Address,
            key: invoiceHeader.Address,
            width: 60
        }, {
            header: invoiceHeader.NumAtCard,
            key: invoiceHeader.NumAtCard,
            width: columnWidth
        }, {
            header: invoiceHeader.Comments,
            key: invoiceHeader.Comments,
            width: columnWidth
        }, {
            header: invoiceHeader.JournalMemo,
            key: invoiceHeader.JournalMemo,
            width: columnWidth
        }, {
            header: invoiceHeader.PaymentGroupCode,
            key: invoiceHeader.PaymentGroupCode,
            width: columnWidth
        }, {
            header: invoiceHeader.SalesPersonCode,
            key: invoiceHeader.SalesPersonCode,
            width: columnWidth
        }, {
            header: invoiceHeader.TaxDate,
            key: invoiceHeader.TaxDate,
            width: columnWidth
        }, {
            header: invoiceHeader.Address2,
            key: invoiceHeader.Address2,
            width: 60
        }, {
            header: invoiceHeader.ControlAccount,
            key: invoiceHeader.ControlAccount,
            width: columnWidth
        }, {
            header: invoiceHeader.U_OrigEntry,
            key: invoiceHeader.U_OrigEntry,
            width: columnWidth
        }, {
            header: invoiceHeader.U_OrigNum,
            key: invoiceHeader.U_OrigNum,
            width: columnWidth
        }
    ];

    // Get Term APIs For DocDue Date


    var key = 'Invoice';
    invoices = jsonObj.QueryResponse[key];

    var docnum = 1000;
    for (var i = 0; i < invoices.length; i++) {
        docnum = docnum + 1;        
        var tmpNumAtCard = '';
        if(invoices[i].hasOwnProperty("CustomField")) {
            if (invoices[i].CustomField.Name == 'P.O. Number' && invoices[i].CustomField.hasOwnProperty("StringValue")) {
                tmpNumAtCard = invoices[i].DocNumber + "#" + invoices[i].CustomField.StringValue;
            }            
        }
        else {
                tmpNumAtCard = invoices[i].DocNumber;
            }

        var Billaddrs = invoices[i].BillAddr.Id ;
         if (invoices[i].BillAddr.Line1 != null) {
            Billaddrs = Billaddrs + " " + invoices[i].BillAddr.Line1
          }
          if (invoices[i].BillAddr.Line2 != null) {
            Billaddrs = Billaddrs + " " + invoices[i].BillAddr.Line2
          }
          if (invoices[i].BillAddr.Line2 != null) {
            Billaddrs = Billaddrs + " " + invoices[i].BillAddr.Line2
          }
          if (invoices[i].BillAddr.Line3 != null) {
            Billaddrs = Billaddrs + " " + invoices[i].BillAddr.Line3
          }
          if (invoices[i].BillAddr.Line4 != null) {
            Billaddrs = Billaddrs + " " + invoices[i].BillAddr.Line4
          }
          if (invoices[i].BillAddr.Line5 != null) {
            Billaddrs = Billaddrs + " " + invoices[i].BillAddr.Line5
          }

        var Shipaddrs = ''
        if (invoices[i].ShipAddr != null) {
            if (invoices[i].ShipAddr.Line1 != null) {

                Shipaddrs = Shipaddrs + " " + invoices[i].ShipAddr.Line1
            }

            Shipaddrs = Shipaddrs + " " + invoices[i].ShipAddr.City + " " + invoices[i].ShipAddr.CountrySubDivisionCode +
                " " + invoices[i].ShipAddr.PostalCode
        }

        terms = jsonObjTerm.QueryResponse["Term"];


        var paymentGroupCode = ''
        var tmpDocDueDays = new Date();
        

        var DocDueDays ="";
        if(invoices[i].hasOwnProperty("DueDate") && invoices[i].DueDate != "" ){
            DocDueDays = invoices[i].DueDate;
            for (var t = terms.length - 1; t >= 0; t--) {
                if (terms[t].Id == invoices[i].SalesTermRef.value) {
                    paymentGroupCode = terms[t].Name;
                    worksheet.addRow({
                        // JournalMemo data not found in QuickBooks
                        DocNum: docnum,
                        DocType: 'dDocument_Items',
                        DocDate: invoices[i].TxnDate,
                        // we need to check data if we are getting from BellSoft

                        DocDueDate: DocDueDays,
                        CardCode: invoices[i].CustomerRef.name,
                        // Bill Address
                        Address: Billaddrs,
                        // same as U_OriginNum + "#" + P.O. Number
                        NumAtCard: tmpNumAtCard,

                        Comments: invoices[i].PrivateNote,
                        JournalMemo: 'Opening Balance',
                        // check in company API
                        PaymentGroupCode: paymentGroupCode,
                        SalesPersonCode: '',
                        TaxDate: invoices[i].TxnDate,
                        // Ship Address
                        Address2: Shipaddrs,

                        ControlAccount: '',
                        U_OrigEntry: invoices[i].DocNumber,
                        U_OrigNum: invoices[i].Id

                    }).commit;
                }
            }
        }
        else {
            for (var t = terms.length - 1; t >= 0; t--) {
            if (terms[t].Id == invoices[i].SalesTermRef.value) {
                var days = terms[t].DueDays;
                var tmpdocDate = new Date(invoices[i].TxnDate);
                paymentGroupCode = terms[t].Name;
              
                if(terms[t].hasOwnProperty("DueDays")){
                    tmpdocDate.setDate(tmpdocDate.getDate() + days);
                      
                    tmpDocDueDays = tmpdocDate.toISOString().slice(0, 10);
                    worksheet.addRow({
                        // JournalMemo data not found in QuickBooks
                        DocNum: docnum,
                        DocType: 'dDocument_Items',
                        DocDate: invoices[i].TxnDate,
                        // we need to check data if we are getting from BellSoft

                        DocDueDate: tmpDocDueDays,
                        CardCode: invoices[i].CustomerRef.name,
                        // Bill Address
                        Address: Billaddrs,
                        // same as U_OriginNum + "#" + P.O. Number
                        NumAtCard: tmpNumAtCard,

                        Comments: invoices[i].CustomerMemo.value,
                        JournalMemo: 'Opening Balance',
                        // check in company API
                        PaymentGroupCode: paymentGroupCode,
                        SalesPersonCode: '',
                        TaxDate: invoices[i].TxnDate,
                        // Ship Address
                        Address2: Shipaddrs,

                        ControlAccount: '',
                        U_OrigEntry: invoices[i].DocNumber,
                        U_OrigNum: invoices[i].DocNumber

                    }).commit;
                }                
            }
            }
        }
    };
    worksheet.commit();
    workbook.commit();
});

// putting all invoice data into excel file (Invoice Items) PCH1
router.post("/getOPCH", function(req, res) {

    var options = {
        useStyles: true,
        useSharedStrings: true
    };

    var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    workbook.zip.pipe(res);
    var worksheet = workbook.addWorksheet("My Sheet");

    var salesInvoiceItem = {
        ParentKey: "ParentKey",
        LineNum: "LineNum",
        ItemCode: "ItemCode",
        Quantity: "Quantity",
        Price: "Price",
        DiscountPercent: "DiscountPercent",
        WarehouseCode: "WarehouseCode",
        TaxCode: "TaxCode",
        WTLiable: "WTLiable",
        LineTotal: "LineTotal",
        UnitPrice: "UnitPrice",
        U_OrigEntry: "U_OrigEntry",
        U_OrigLine: "U_OrigLine",
        Description: "Description"
    };

    worksheet.columns = [{
            header: salesInvoiceItem.ParentKey,
            key: salesInvoiceItem.ParentKey,
            width: columnWidth
        }, {
            header: salesInvoiceItem.LineNum,
            key: salesInvoiceItem.LineNum,
            width: columnWidth
        }, {
            header: salesInvoiceItem.ItemCode,
            key: salesInvoiceItem.ItemCode,
            width: columnWidth
        }, {
            header: salesInvoiceItem.Quantity,
            key: salesInvoiceItem.Quantity,
            width: columnWidth
        }, {
            header: salesInvoiceItem.Price,
            key: salesInvoiceItem.Price,
            width: columnWidth
        }, {
            header: salesInvoiceItem.DiscountPercent,
            key: salesInvoiceItem.DiscountPercent,
            width: columnWidth
        }, {
            header: salesInvoiceItem.WarehouseCode,
            key: salesInvoiceItem.WarehouseCode,
            width: columnWidth
        }, {
            header: salesInvoiceItem.TaxCode,
            key: salesInvoiceItem.TaxCode,
            width: columnWidth
        }, {
            header: salesInvoiceItem.WTLiable,
            key: salesInvoiceItem.WTLiable,
            width: columnWidth
        }, {
            header: salesInvoiceItem.LineTotal,
            key: salesInvoiceItem.LineTotal,
            width: columnWidth
        },
        {
            header: salesInvoiceItem.UnitPrice,
            key: salesInvoiceItem.UnitPrice,
            width: columnWidth
        }, {
            header: salesInvoiceItem.U_OrigEntry,
            key: salesInvoiceItem.U_OrigEntry,
            width: columnWidth
        }, {
            header: salesInvoiceItem.U_OrigLine,
            key: salesInvoiceItem.U_OrigLine,
            width: columnWidth
        }, {
            header: salesInvoiceItem.Description,
            key: salesInvoiceItem.Description,
            width: columnWidth
        }
    ];

    var key = 'Invoice';

    invoices = jsonObj.QueryResponse.Invoice;
    var docnumber = 1000
    for (var i = 0; i < invoices.length; i++) {
        docnumber = docnumber + 1;
        var Line_key = 'Line';
        var Lines = invoices[i][Line_key];
        // console.log('Length of Line Item ',Lines.length);
        for (var j = 0; j < Lines.length - 1; j++) {

            //Description if exists
            if (Lines[j].hasOwnProperty("Description")) {
                var Description = Lines[j].Description;
            } else {
                var Description = "";
            }

            //Amount if Exist
            if(Lines[j].hasOwnProperty("Amount")){
                var amount = Lines[j].Amount;
            }
            else{
                var amount = '';
            }

            var detailType = Lines[j]["DetailType"];
            if (detailType.toString() === "SalesItemLineDetail") {
                //ItemCode if Exists
                if(Lines[j]["SalesItemLineDetail"].hasOwnProperty("ItemRef")){
                    var itemCode = Lines[j]["SalesItemLineDetail"].ItemRef.name
                }
                else{
                     var itemCode = '';
                }
                //Quantity if Exists
                if(Lines[j]["SalesItemLineDetail"].hasOwnProperty("Qty")){
                    var quantity = Lines[j]["SalesItemLineDetail"].Qty;
                }
                else{
                    var quantity = '';
                }
                //Unit Price If Exists
                if(Lines[j]["SalesItemLineDetail"].hasOwnProperty("UnitPrice")){
                    var UnitPrice = Lines[j]["SalesItemLineDetail"].UnitPrice;
                }
                else{
                    var UnitPrice = '';
                }
            }
            else if(detailType.toString() === "SubTotalLineDetail") {
                var itemCode = '';
                var quantity = '';
                var UnitPrice = '';
            }
                worksheet.addRow({
                        ParentKey: docnumber,
                        LineNum: '',
                        ItemCode: itemCode,
                        Quantity: quantity,
                        Price: UnitPrice,
                        DiscountPercent: '0',
                        WarehouseCode: '',
                        TaxCode: '',
                        WTLiable: 'tNo',
                        LineTotal: amount,
                        UnitPrice: UnitPrice,
                        U_OrigEntry: invoices[i].DocNumber,
                        U_OrigLine: Lines[j].LineNum,
                        Description: Description
                    }).commit;
        }
    }
    worksheet.commit();
    workbook.commit();
});

module.exports = router;