
var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
http          = require('http'),
port          = process.env.PORT || 3000,
request       = require('request'),
qs            = require('querystring'),
util          = require('util');
var path          = require('path');
var router = express.Router(),
QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
jsonObj = {'key':'value'},
columnWidth = 30;

router.post("/getjournalEntry", function(req, res) {

  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet_ojdt = workbook.addWorksheet("OJDT");
  var worksheet_jdt1 = workbook.addWorksheet("JDT1");

  //OJDT WorkSheet
  var journalHeader = {
    JdtNum: "JdtNum",
    ReferenceDate: "ReferenceDate",
    Memo: "Memo",
    Reference: "Reference",
    Reference2: "Reference2",
    TaxDate: "TaxDate",
    Series: "Series",
    DueDate:"DueDate",
    U_OrigEntry:"U_OrigEntry",
    U_OrigNum:"U_OrigNum"
  };
  
  worksheet_ojdt.columns = [
    { header: journalHeader.JdtNum,          key: journalHeader.JdtNum,         width: columnWidth },
    { header: journalHeader.ReferenceDate,   key: journalHeader.ReferenceDate,  width: columnWidth },
    { header: journalHeader.Memo,            key: journalHeader.Memo,           width: columnWidth },
    { header: journalHeader.Reference,       key: journalHeader.Reference,      width: columnWidth },
    { header: journalHeader.Reference2,      key: journalHeader.Reference2,     width: columnWidth },
    { header: journalHeader.TaxDate,         key: journalHeader.TaxDate,        width: columnWidth },
    { header: journalHeader.Series,          key: journalHeader.Series,         width: columnWidth },
    { header: journalHeader.DueDate,         key: journalHeader.DueDate,        width: columnWidth },
    { header: journalHeader.U_OrigEntry,     key: journalHeader.U_OrigEntry,    width: columnWidth },
    { header: journalHeader.U_OrigNum,       key: journalHeader.U_OrigNum,      width: columnWidth }
  ];

  //JDT1 WorkSheet
  var jdt1Item = {
    ParentKey: "ParentKey",
    LineNum: "LineNum",
    Line_ID: "Line_ID",
    AccountCode: "AccountCode",
    Debit: "Debit",
    Credit: "Credit",
    DueDate: "DueDate",
    ShortName: "ShortName",
    LineMemo: "LineMemo",
    ReferenceDate1: "ReferenceDate1",
    Reference1: "Reference1",
    Reference2: "Reference2",
    TaxDate: "TaxDate",
    ControlAccount: "ControlAccount",
    U_OrigEntry: "U_OrigEntry",
    U_OrigLine:"U_OrigLine"
  };

  worksheet_jdt1.columns = [
    { header: jdt1Item.ParentKey,        key: jdt1Item.ParentKey,       width: columnWidth },
    { header: jdt1Item.LineNum,          key: jdt1Item.LineNum,         width: columnWidth },
    { header: jdt1Item.Line_ID,          key: jdt1Item.Line_ID,         width: columnWidth },
    { header: jdt1Item.AccountCode,      key: jdt1Item.AccountCode,     width: columnWidth },
    { header: jdt1Item.Debit,            key: jdt1Item.Debit,           width: columnWidth },
    { header: jdt1Item.Credit,           key: jdt1Item.Credit,          width: columnWidth },
    { header: jdt1Item.DueDate,          key: jdt1Item.DueDate,         width: columnWidth },
    { header: jdt1Item.ShortName,        key: jdt1Item.ShortName,       width: columnWidth },
    { header: jdt1Item.LineMemo,         key: jdt1Item.LineMemo,        width: columnWidth },
    { header: jdt1Item.ReferenceDate1,   key: jdt1Item.ReferenceDate1,  width: columnWidth },
    { header: jdt1Item.Reference1,       key: jdt1Item.Reference1,      width: columnWidth },
    { header: jdt1Item.Reference2,       key: jdt1Item.Reference2,      width: columnWidth },
    { header: jdt1Item.TaxDate,          key: jdt1Item.TaxDate,         width: columnWidth },
    { header: jdt1Item.ControlAccount,   key: jdt1Item.ControlAccount,  width: columnWidth },
    { header: jdt1Item.U_OrigEntry,      key: jdt1Item.U_OrigEntry,     width: columnWidth },
    { header: jdt1Item.U_OrigLine,       key: jdt1Item.U_OrigLine,      width: columnWidth }
  ];
   
  qbo.findJournalEntries([
    {field: 'TxnDate', value: '2017-08-01', operator: '>='},
    {field: 'TxnDate', value: '2017-09-30', operator: '<='},
    {field: 'limit', value: 200}], function(err, journalEntries) {
      jsonObj = journalEntries;
      var key = 'JournalEntry';
      var journals = {"":""};
      journals =  jsonObj.QueryResponse[key];

      var docnum = 1000;
      for (var i = 0 ; i <= journals.length  - 1 ; i++) {
        //dynamically increment
        docnum += 1;
        //pre-initialise field details if condition fails
        var memo = '';


        //fill memo field
         if(journals[i].hasOwnProperty("PrivateNote")){
          memo = journals[i].PrivateNote;
         }

        //fillthe header worksheet
          worksheet_ojdt.addRow({
            JdtNum: docnum,
            ReferenceDate: journals[i].TxnDate,
            Memo: memo,
            Reference: "",
            Reference2: "",
            TaxDate: journals[i].TxnDate,
            Series: "",
            DueDate: journals[i].TxnDate,
            U_OrigEntry: journals[i].Id,
            U_OrigNum: journals[i].DocNumber 
          }).commit;

        //vpm1 or vpm2or vpm4 
        var Line_key = 'Line';
        var Lines = journals[i][Line_key];
         for (var j = 0; j <= Lines.length - 1; j++) {
          var debit = "", credit = "", accountCode = "";
            if(Lines[j].DetailType == "JournalEntryLineDetail" && Lines[j].JournalEntryLineDetail.hasOwnProperty("PostingType") ){
              //fill up AccountCode
              if(Lines[j].JournalEntryLineDetail.hasOwnProperty("AccountRef")){
                accountCode = Lines[j].JournalEntryLineDetail.AccountRef.name;
              }
              //Fill up the debit or credit fields
              if(Lines[j].JournalEntryLineDetail.PostingType == "Credit"){
                credit = Lines[j].Amount;
              }
              else if(Lines[j].JournalEntryLineDetail.PostingType == "Debit"){
                debit = Lines[j].Amount;
              }
            }
                worksheet_jdt1.addRow({
                  ParentKey: docnum,
                  LineNum: "",
                  Line_ID: "",
                  AccountCode: accountCode,
                  Debit: debit,
                  Credit: credit,
                  DueDate: "",
                  ShortName: Lines[j].Description,
                  LineMemo: "",
                  ReferenceDate1: "",
                  Reference1: "",
                  Reference2: "",
                  TaxDate: "",
                  ControlAccount: "",
                  U_OrigEntry: journals[i].Id,
                  U_OrigLine: Lines[j].Id
                }).commit; 
          }
      }
    worksheet_ojdt.commit();
    worksheet_jdt1.commit();
    workbook.commit();  
  });
});

module.exports = router;