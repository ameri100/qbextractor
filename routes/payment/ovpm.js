
var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
    http          = require('http'),
    port          = process.env.PORT || 3000,
    request       = require('request'),
    qs            = require('querystring'),
    util          = require('util');
var path          = require('path');
var router = express.Router(),
  QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
    tmpEmployeeInfo = {'key':'value'},
    jsonObj = {'key':'value'},
    columnWidth = 30;

// OVPM (for both)
router.post("/getPayment", function(req, res) {

var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet = workbook.addWorksheet("OVPM_purchase");

var paymentHeader = {
   DocNum : "DocNum",
  DocType: "DocType",
  DocDate: "DocDate",
  CardCode: "CardCode",
  CardName: "CardName",
  CashAccount: "CashAccount",
  DocCurrency: "DocCurrency",
  CashSum: "CashSum",
  CheckAccount:"CheckAccount",
  TransferAccount:"TransferAccount",
  TransferSum:"TransferSum",
  TransferDate:"TransferDate",
  TransferReference:"TransferReference",
  DocRate:"DocRate",
  Remarks: "Remarks",
  TaxDate:"TaxDate",
  BankCode:"BankCode",
  BankAccount:"BankAccount",
  DocObjectCode:"DocObjectCode",
  DueDate: "DueDate",
  LocationCode:"LocationCode",
  U_OrigEntry:"U_OrigEntry",
  U_OrigNum:"U_OrigNum"
};
  
  worksheet.columns = [
    { header: paymentHeader.DocNum,       key: paymentHeader.DocNum,      width: columnWidth },
    { header: paymentHeader.DocType,      key: paymentHeader.DocType,     width: columnWidth },
    { header: paymentHeader.DocDate,      key: paymentHeader.DocDate,     width: columnWidth },
    { header: paymentHeader.CardCode,     key: paymentHeader.CardCode,    width: columnWidth },
    { header: paymentHeader.CardName, key: paymentHeader.CardName,   width: columnWidth },
    { header: paymentHeader.CashAccount, key: paymentHeader.CashAccount,   width: columnWidth },
    { header: paymentHeader.DocCurrency,key: paymentHeader.DocCurrency,  width: columnWidth },
    { header: paymentHeader.CashSum,      key: paymentHeader.CashSum,     width: columnWidth },
    { header: paymentHeader.CheckAccount,      key: paymentHeader.CheckAccount,     width: columnWidth },
    { header: paymentHeader.TransferAccount,      key: paymentHeader.TransferAccount,     width: columnWidth },
    { header: paymentHeader.TransferSum,      key: paymentHeader.TransferSum,     width: columnWidth },
    { header: paymentHeader.TransferDate,      key: paymentHeader.TransferDate,     width: columnWidth },
    { header: paymentHeader.TransferReference,      key: paymentHeader.TransferReference,     width: columnWidth },
    { header: paymentHeader.DocRate,      key: paymentHeader.DocRate,     width: columnWidth },
    { header: paymentHeader.Remarks,      key: paymentHeader.Remarks,     width: columnWidth },
    { header: paymentHeader.TaxDate,      key: paymentHeader.TaxDate,    width: columnWidth },
    { header: paymentHeader.DocObjectCode,key: paymentHeader.DocObjectCode, width: columnWidth },
    { header: paymentHeader.DueDate,      key: paymentHeader.DueDate,   width: columnWidth },
    { header: paymentHeader.LocationCode, key: paymentHeader.LocationCode,   width: columnWidth },
    { header: paymentHeader.U_OrigEntry, key: paymentHeader.U_OrigEntry,   width: columnWidth },
    { header: paymentHeader.U_OrigNum, key: paymentHeader.U_OrigNum,   width: columnWidth },
    
  ];

qbo.findBillPayments('', function(err, billPayment) {
    var obj = {'key':'value'};
 console.log(
             /* define stringify */
             //JSON.stringify(invoice)
 );
  //  console.log(employee);
    jsonObj = billPayment;
    var key = 'BillPayment';

var payments = {"":""};
payments =  jsonObj.QueryResponse[key];
console.log('Length of Employee',payments.length);

for (var i = 0 ; i < payments.length ; i++) {
  
  worksheet.addRow({
    // JournalMemo data not found in QuickBooks
      DocNum: payments[i].DocNumber,
      DocType: 'rSupplier',
      DocDate: payments[i].TxnDate,
      CardCode: '',
      CardName:payments[i].VendorRef.name,
      CashAccount:'',
      DocCurrency: 'tYES',
      CashSum:'0',
      CheckAccount:'',
      TransferAccount:'',
      TransferSum:payments[i].TotalAmt,
      TransferDate:payments[i].TxnDate,
      TransferReference:'',
      DocRate:'0',
      Remarks: '',
      TaxDate: payments[i].TxnDate,
      DocObjectCode: 'bopot_OutgoingPayments',
      DueDate: '',
     // DocType DueDate, Remarks, LocationCode, CounterReference we dont know
     LocationCode:'',  
     U_OrigEntry:payments[i].DocNumber,
     U_OrigNum:payments[i].DocNumber 
  }).commit;

};
  
  worksheet.commit();
  workbook.commit();
})

});


router.post("/getPayment_Check", function(req, res) {

var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet = workbook.addWorksheet("VPM1_purchase");

var paymentHeader = {
  DocNum : "DocNum",
  LineNum: "LineNum",
  DueDate: "DueDate",
  CheckNumber: "CheckNumber",
  BankCode: "BankCode",
  Branch: "Branch",
  AccountNum: "AccountNum",
  Trnsfrable: "Trnsfrable",
  CheckSum :"CheckSum",
  CountryCode:"CountryCode",
  CheckAccount:"CheckAccount",
  ManualCheck:"ManualCheck"
};
  
  worksheet.columns = [
    { header: paymentHeader.DocNum,       key: paymentHeader.DocNum,      width: columnWidth },
    { header: paymentHeader.LineNum,      key: paymentHeader.LineNum,     width: columnWidth },
    { header: paymentHeader.DueDate,      key: paymentHeader.DueDate,     width: columnWidth },
    { header: paymentHeader.CheckNumber,  key: paymentHeader.CheckNumber, width: columnWidth },
    { header: paymentHeader.BankCode,     key: paymentHeader.BankCode,    width: columnWidth },
    { header: paymentHeader.Branch,       key: paymentHeader.Branch,      width: columnWidth },
    { header: paymentHeader.AccountNum,   key: paymentHeader.AccountNum,  width: columnWidth },
    { header: paymentHeader.Trnsfrable,   key: paymentHeader.Trnsfrable,  width: columnWidth },
    { header: paymentHeader.CheckSum,     key: paymentHeader.CheckSum,    width: columnWidth },
    { header: paymentHeader.CountryCode,  key: paymentHeader.CountryCode, width: columnWidth },
    { header: paymentHeader.CheckAccount, key: paymentHeader.CheckAccount,width: columnWidth },
    { header: paymentHeader.ManualCheck,  key: paymentHeader.ManualCheck, width: columnWidth }
  ];

qbo.findBillPayments('', function(err, billPayment) {
    var obj = {'key':'value'};
 console.log(
             /* define stringify */
             //JSON.stringify(invoice)
 );
  //  console.log(employee);
    jsonObj = billPayment;
    var key = 'BillPayment';

var payments = {"":""};
payments =  jsonObj.QueryResponse[key];
console.log('Length of Employee',payments.length);

for (var i = 0 ; i < payments.length ; i++) {
  
  worksheet.addRow({
    // JournalMemo data not found in QuickBooks
      DocNum: payments[i].DocNumber,
      // Line no should be incremental
      LineNum: '',
      DueDate:payments[i].TxnDate,
      CheckNumber: '',
      BankCode: '',
      Branch: '',
      AccountNum: '',
      Trnsfrable:'tNO',
      CheckSum:'',
      CountryCode: '',
      CheckAccount:'',
      ManualCheck: 'tYes' 
  }).commit;

};
  
  worksheet.commit();
  workbook.commit();
})
});

// BT = Bank Transfer
router.post("/getPayment_BT", function(req, res) {

var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet = workbook.addWorksheet("VPM4_purchase");

var paymentHeader = {
   DocNum : "DocNum",
  LineNum: "LineNum",
  AccountCode: "AccountCode",
  SumPaid: "SumPaid",
  Description: "Description",
  AccountName: "AccountName",
    OcrCode1: "ProfitCenter1",
  OcrCode2: "ProfitCenter2"
};
  
  worksheet.columns = [
    { header: paymentHeader.DocNum,       key: paymentHeader.DocNum,      width: columnWidth },
    { header: paymentHeader.LineNum,      key: paymentHeader.LineNum,     width: columnWidth },
    { header: paymentHeader.AccountCode,  key: paymentHeader.AccountCode,    width: columnWidth },
    { header: paymentHeader.SumPaid,      key: paymentHeader.SumPaid,    width: columnWidth },
    { header: paymentHeader.Description,  key: paymentHeader.Description,     width: columnWidth },
    { header: paymentHeader.AccountName,key: paymentHeader.AccountName,  width: columnWidth },
       { header: paymentHeader.OcrCode1,      key: paymentHeader.OcrCode1,     width: columnWidth },
    { header: paymentHeader.OcrCode2,      key: paymentHeader.OcrCode2,     width: columnWidth }
  ];

qbo.findBillPayments('', function(err, billPayment) {
    var obj = {'key':'value'};
 console.log(
             /* define stringify */
             //JSON.stringify(invoice)
 );
  //  console.log(employee);
    jsonObj = billPayment;
    var key = 'BillPayment';

var payments = {"":""};
payments =  jsonObj.QueryResponse[key];
console.log('Length of Employee',payments.length);

for (var i = 0 ; i < payments.length ; i++) {
  
  worksheet.addRow({
    // JournalMemo data not found in QuickBooks
      DocNum: payments[i].DocNumber,
      LineNum: '',
    AccountCode:'',
    SumPaid:payments[i].TotalAmt,
    Description:'',
    AccountName:'',
    OcrCode1:'',
    OcrCode2:''
  }).commit;

};
  
  worksheet.commit();
  workbook.commit();
})
});


module.exports = router;