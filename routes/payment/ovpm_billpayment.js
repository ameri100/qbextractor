
var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
http          = require('http'),
port          = process.env.PORT || 3000,
request       = require('request'),
qs            = require('querystring'),
util          = require('util');
var path          = require('path');
var router = express.Router(),
QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
jsonObj = {'key':'value'},
columnWidth = 30;

router.post("/getbillPayment", function(req, res) {

  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet_ovpm = workbook.addWorksheet("OVPM");
  var worksheet_vpm1 = workbook.addWorksheet("VPM1");
  var worksheet_vpm2 = workbook.addWorksheet("VPM2");
  var worksheet_vpm4 = workbook.addWorksheet("VPM4");

  //OVPM WorkSheet
  var paymentHeader = {
    DocNum : "DocNum",
    DocType: "DocType",
    DocDate: "DocDate",
    CardCode: "CardCode",
    CardName: "CardName",
    CashAccount: "CashAccount",
    DocCurrency: "DocCurrency",
    CashSum: "CashSum",
    CheckAccount:"CheckAccount",
    TransferAccount:"TransferAccount",
    TransferSum:"TransferSum",
    TransferDate:"TransferDate",
    TransferReference:"TransferReference",
    DocRate:"DocRate",
    Remarks: "Remarks",
    TaxDate:"TaxDate",
    BankCode:"BankCode",
    BankAccount:"BankAccount",
    DocObjectCode:"DocObjectCode",
    DueDate: "DueDate",
    LocationCode:"LocationCode",
    U_OrigEntry:"U_OrigEntry",
    U_OrigNum:"U_OrigNum"
  };
  
  worksheet_ovpm.columns = [
    { header: paymentHeader.DocNum,           key: paymentHeader.DocNum,          width: columnWidth },
    { header: paymentHeader.DocType,          key: paymentHeader.DocType,         width: columnWidth },
    { header: paymentHeader.DocDate,          key: paymentHeader.DocDate,         width: columnWidth },
    { header: paymentHeader.CardCode,         key: paymentHeader.CardCode,        width: columnWidth },
    { header: paymentHeader.CardName,         key: paymentHeader.CardName,        width: columnWidth },
    { header: paymentHeader.CashAccount,      key: paymentHeader.CashAccount,     width: columnWidth },
    { header: paymentHeader.DocCurrency,      key: paymentHeader.DocCurrency,     width: columnWidth },
    { header: paymentHeader.CashSum,          key: paymentHeader.CashSum,         width: columnWidth },
    { header: paymentHeader.CheckAccount,     key: paymentHeader.CheckAccount,    width: columnWidth },
    { header: paymentHeader.TransferAccount,  key: paymentHeader.TransferAccount, width: columnWidth },
    { header: paymentHeader.TransferSum,      key: paymentHeader.TransferSum,     width: columnWidth },
    { header: paymentHeader.TransferDate,     key: paymentHeader.TransferDate,    width: columnWidth },
    { header: paymentHeader.TransferReference,key: paymentHeader.TransferReference,width: columnWidth },
    { header: paymentHeader.DocRate,          key: paymentHeader.DocRate,         width: columnWidth },
    { header: paymentHeader.Remarks,          key: paymentHeader.Remarks,         width: columnWidth },
    { header: paymentHeader.TaxDate,          key: paymentHeader.TaxDate,         width: columnWidth },
    { header: paymentHeader.BankCode,         key: paymentHeader.BankCode,         width: columnWidth },
    { header: paymentHeader.BankAccount,      key: paymentHeader.BankAccount,      width: columnWidth },
    { header: paymentHeader.DocObjectCode,    key: paymentHeader.DocObjectCode,   width: columnWidth },
    { header: paymentHeader.DueDate,          key: paymentHeader.DueDate,         width: columnWidth },
    { header: paymentHeader.LocationCode,     key: paymentHeader.LocationCode,    width: columnWidth },
    { header: paymentHeader.U_OrigEntry,      key: paymentHeader.U_OrigEntry,     width: columnWidth },
    { header: paymentHeader.U_OrigNum,        key: paymentHeader.U_OrigNum,       width: columnWidth }
  ];

  //OVPM1 WorkSheet
  var vpm1Item = {
    ParentKey: "ParentKey",
    LineNum: "LineNum",
    DueDate: "DueDate",
    CheckNumber: "CheckNumber",
    BankCode: "BankCode",
    Branch: "Branch",
    AccounttNum: "AccounttNum",
    Trnsfrable: "Trnsfrable",
    CheckSum: "CheckSum",
    CountryCode: "CountryCode",
    CheckAccount: "CheckAccount",
    ManualCheck: "ManualCheck",
    U_OrigEntry: "U_OrigEntry",
    U_OrigLine:"U_OrigLine"
  };

  worksheet_vpm1.columns = [
    { header: vpm1Item.ParentKey,        key: vpm1Item.ParentKey,       width: columnWidth },
    { header: vpm1Item.LineNum,          key: vpm1Item.LineNum,         width: columnWidth },
    { header: vpm1Item.DueDate,          key: vpm1Item.DueDate,         width: columnWidth },
    { header: vpm1Item.CheckNumber,      key: vpm1Item.CheckNumber,     width: columnWidth },
    { header: vpm1Item.BankCode,         key: vpm1Item.BankCode,        width: columnWidth },
    { header: vpm1Item.Branch,           key: vpm1Item.Branch,          width: columnWidth },
    { header: vpm1Item.AccounttNum,      key: vpm1Item.AccounttNum,     width: columnWidth },
    { header: vpm1Item.Trnsfrable,       key: vpm1Item.Trnsfrable,      width: columnWidth },
    { header: vpm1Item.CheckSum,         key: vpm1Item.CheckSum,        width: columnWidth },
    { header: vpm1Item.CountryCode,      key: vpm1Item.CountryCode,     width: columnWidth },
    { header: vpm1Item.CheckAccount,     key: vpm1Item.CheckAccount,    width: columnWidth },
    { header: vpm1Item.ManualCheck,      key: vpm1Item.ManualCheck,     width: columnWidth },
    { header: vpm1Item.U_OrigEntry,      key: vpm1Item.U_OrigEntry,     width: columnWidth },
    { header: vpm1Item.U_OrigLine,       key: vpm1Item.U_OrigLine,      width: columnWidth }
  ];

  //OVPM2 WorkSheet
  var vpm2Item = {
    ParentKey: "ParentKey",
    LineNum: "LineNum",
    DocEntry: "DocEntry",
    SumApplied: "SumApplied",
    InvoiceType: "InvoiceType",
    TransactionType: "TransactionType",
    "InvDocNum(Invoice_Id)": "InvDocNum(Invoice_Id)",
    U_OrigEntry: "U_OrigEntry",
    U_OrigLine:"U_OrigLine"
  };

  worksheet_vpm2.columns = [
    { header: vpm2Item.ParentKey,        key: vpm2Item.ParentKey,       width: columnWidth },
    { header: vpm2Item.LineNum,          key: vpm2Item.LineNum,         width: columnWidth },
    { header: vpm2Item.DocEntry,         key: vpm2Item.DocEntry,        width: columnWidth },
    { header: vpm2Item.SumApplied,       key: vpm2Item.SumApplied,      width: columnWidth },
    { header: vpm2Item.InvoiceType,      key: vpm2Item.InvoiceType,     width: columnWidth },
  { header: vpm2Item.TransactionType,          key: vpm2Item.TransactionType, width: columnWidth },
  { header: vpm2Item["InvDocNum(Invoice_Id)"],        key: vpm2Item["InvDocNum(Invoice_Id)"],       width: columnWidth },
    { header: vpm2Item.U_OrigEntry,      key: vpm2Item.U_OrigEntry,     width: columnWidth },
    { header: vpm2Item.U_OrigLine,        key: vpm2Item.U_OrigLine,       width: columnWidth }
  ];

  //OVPM4 WorkSheet  
  var vpm4Item = {
    ParentKey: "ParentKey",
    LineNum: "LineNum",
    AccountCode: "AccountCode",
    SumPaid: "SumPaid",
    Description: "Description",
    AccountName: "AccountName",        
    ProfitCenter: "ProfitCenter",
    ProfitCenter2: "ProfitCenter2",
    U_OrigEntry: "U_OrigEntry",
    U_OrigLine:"U_OrigLine"
  };

  worksheet_vpm4.columns = [
    { header: vpm4Item.ParentKey,        key: vpm4Item.ParentKey,       width: columnWidth },
    { header: vpm4Item.LineNum,          key: vpm4Item.LineNum,         width: columnWidth },
    { header: vpm4Item.AccountCode,      key: vpm4Item.AccountCode,     width: columnWidth },
    { header: vpm4Item.SumPaid,          key: vpm4Item.SumPaid,         width: columnWidth },
    { header: vpm4Item.Description,      key: vpm4Item.Description,     width: columnWidth },
    { header: vpm4Item.AccountName,      key: vpm4Item.AccountName,     width: columnWidth },
    { header: vpm4Item.ProfitCenter,     key: vpm4Item.ProfitCenter,    width: columnWidth },
    { header: vpm4Item.ProfitCenter2,    key: vpm4Item.ProfitCenter2,   width: columnWidth },
    { header: vpm4Item.U_OrigEntry,      key: vpm4Item.U_OrigEntry,     width: columnWidth },
    { header: vpm4Item.U_OrigLine,       key: vpm4Item.U_OrigLine,      width: columnWidth }
  ];
  
  qbo.findBillPayments([
    {field: 'TxnDate', value: '2017-08-01', operator: '>='},
    {field: 'TxnDate', value: '2017-09-30', operator: '<='},
    {field: 'limit', value: 1000}], function(err, billPayments) {
      jsonObj = billPayments;
      var key = 'BillPayment';
      var payments = {"":""};
      payments =  jsonObj.QueryResponse[key];

      var docnum = 1000;
      for (var i = 0 ; i < payments.length; i++) {
        //dynamically increment
        docnum += 1;
        //pre-initialise field details if condition fails
        var TransferAccount = "", 
            TransferSum = "", 
            TransferDate = "",
            cashSum = "",
            checkAccount = "",
            cashAccount = "",
            CardCode = "",
            account = "",
            transferReference = "",
            docRate = "1",
            comments = "",
            U_OrigLine = "";
        //get card code
        if(payments[i].hasOwnProperty("VendorRef") ){ 
          if(payments[i].VendorRef.name){
            var CardCode = payments[i].VendorRef.name;
          }        
        }
        //get checkaccount 
        if(payments[i].hasOwnProperty("PayType") && payments[i].PayType == "Check"){
          if(payments[i].hasOwnProperty("DocNumber")){
            if(payments[i].hasOwnProperty("CheckPayment") && payments[i].CheckPayment.hasOwnProperty("BankAccountRef")){
              if(payments[i].CheckPayment.BankAccountRef.hasOwnProperty("name")){
                 checkAccount = payments[i].CheckPayment.BankAccountRef.name;
              }
            }
          }
          else{             
            if(payments[i].hasOwnProperty("CheckPayment") && payments[i].CheckPayment.hasOwnProperty("BankAccountRef")){
              if(payments[i].CheckPayment.BankAccountRef.hasOwnProperty("name")){
                 TransferAccount = payments[i].CheckPayment.BankAccountRef.name;
                  TransferSum = payments[i].TotalAmt,
                  TransferDate = payments[i].TxnDate
              }
            }
          }
        }

        //ref number
        if(payments[i].hasOwnProperty("DocNumber")){
          transferReference = payments[i].DocNumber;
        }

        //Remarks
        if(payments[i].hasOwnProperty("PrivateNote")){
          var comments = payments[i].PrivateNote;
        } 
            
    //fillthe header worksheet
      worksheet_ovpm.addRow({
        DocNum : docnum,
        DocType: "rSupplier",
        DocDate: payments[i].TxnDate,
        CardCode: CardCode,
        CardName: CardCode,
        CashAccount: cashAccount,
        DocCurrency: payments[i].CurrencyRef.value,
        CashSum: cashSum,
        CheckAccount: checkAccount,
        TransferAccount: TransferAccount,
        TransferSum: TransferSum,
        TransferDate: TransferDate,
        TransferReference: transferReference,
        DocRate: docRate,
        Remarks: comments,
        TaxDate: payments[i].TxnDate,
        BankCode: "",
        BankAccount: "",
        DocObjectCode:"bopot_OutgoingPayments",
        DueDate: "",
        LocationCode:"",
        U_OrigEntry: payments[i].Id,
        U_OrigNum: payments[i].Id   
      }).commit;
     // console.log(docnum);
    //vpm1 or vpm2or vpm4 
      var Line_key = 'Line';
      var Lines = payments[i][Line_key];
       for (var j = 0; j <= Lines.length - 1; j++) {
          //check for U_origLine
            if(Lines[j].hasOwnProperty("Id")){
              var U_OrigLine = Lines[j].Id;
            }
          //check for LinkedTxn
          if(Lines[j].hasOwnProperty("LinkedTxn")){ 
            //check if property TxnType exists
            if(Lines[j]["LinkedTxn"][0].hasOwnProperty("TxnType")){
              if(Lines[j]["LinkedTxn"][0].TxnType == "Bill" || Lines[j]["LinkedTxn"][0].TxnType == "JournalEntry"){
                var LTxnType = Lines[j]["LinkedTxn"][0].TxnType;
                var TxnId = Lines[j]["LinkedTxn"][0].TxnId;

                if(payments[i].hasOwnProperty("PayType") && payments[i].PayType == "Check"){
                  //make entry in vpm2 
                  worksheet_vpm2.addRow({
                    ParentKey: docnum,
                    LineNum: "",
                    DocEntry: "",
                    SumApplied: Lines[j].Amount,
                    InvoiceType: "18",
                    TransactionType: LTxnType,
                    "InvDocNum(Invoice_Id)": TxnId, 
                    U_OrigEntry: payments[i].Id,
                    U_OrigLine: U_OrigLine                   
                  }).commit;
                }
            }            
              else{
                worksheet_vpm4.addRow({
                  ParentKey: docnum,
                  LineNum: "",
                  AccountCode: "",
                  SumPaid: Lines[j].Amount,
                  Description: "",
                  AccountName: account,        
                  ProfitCenter: "",
                  ProfitCenter2: "",
                  U_OrigEntry: payments[i].Id,
                  U_OrigNum: U_OrigLine  
                }).commit;
              }
            }
          }
          if(payments[i].PayType == "Check" && payments[i].hasOwnProperty("DocNumber")){
            worksheet_vpm1.addRow({
              ParentKey: docnum,
              LineNum: "",
              DueDate: "",
              CheckNumber: payments[i].DocNumber,
              BankCode: "",
              Branch: "",
              AccounttNum: "",
              Trnsfrable: "tNO",
              CheckSum: Lines[j].Amount,
              CountryCode: "US",
              CheckAccount: checkAccount,
              ManualCheck: "tYES",
              U_OrigEntry: payments[i].Id,              
              U_OrigLine: U_OrigLine
            }).commit;
          }
        }
      }
    worksheet_ovpm.commit();
    worksheet_vpm1.commit();
    worksheet_vpm2.commit();
    worksheet_vpm4.commit();
    workbook.commit();  
  });
});

module.exports = router;