
var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
http          = require('http'),
port          = process.env.PORT || 3000,
request       = require('request'),
qs            = require('querystring'),
util          = require('util');
var path          = require('path');
var router = express.Router(),
QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
tmpEmployeeInfo = {'key':'value'},
jsonObj = {'key':'value'},
jsonObjPay = {'key':'value'},
jsonObjAcc = {'key':'value'},
columnWidth = 30;

router.post("/getVendor", function(req, res) {

  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet_ocrd = workbook.addWorksheet("OCRD");
  var worksheet_crd1 = workbook.addWorksheet("CRD1");
 
  //OVPM WorkSheet
  var vendorHeader = {
    CardCode : "CardCode",
    CardName: "CardName",
    CardForeignName: "CardForeignName",
    CardType: "CardType",
    GroupCode: "GroupCode",
    Phone1: "Phone1",
    Phone2: "Phone2",
    Fax: "Fax",
    Cellular:"Cellular",
    EmailAddress:"EmailAddress",
    Pager:"Pager",
    Website:"Website",
    PayTermsGrpCode:"PayTermsGrpCode",
    CreditLimit:"CreditLimit",
    FreeText: "FreeText",
    SalesPersonCode:"SalesPersonCode",
    Currency:"Currency",
    SubjectToWithholdingTax:"SubjectToWithholdingTax",
    WTCode:"WTCode",
    VATNo: "VATNo",
    CSTNo:"CSTNo",
    PAN:"PAN",
    ServiceTaxNo:"ServiceTaxNo",
    ControlAccount: "ControlAccount",
    IsSubcontractor:"IsSubcontractor",
    U_OrigNum:"U_OrigNum",
    "IsVendor": "IsVendor",
    CompanyName : "CompanyName"
  };  

  worksheet_ocrd.columns = [
  { header: vendorHeader.CardCode,           key: vendorHeader.CardCode,              width: columnWidth },
  { header: vendorHeader.CardName,           key: vendorHeader.CardName,              width: columnWidth },
  { header: vendorHeader.CardForeignName,    key: vendorHeader.CardForeignName,       width: columnWidth },
  { header: vendorHeader.CardType,           key: vendorHeader.CardType,              width: columnWidth },
  { header: vendorHeader.GroupCode,          key: vendorHeader.GroupCode,             width: columnWidth },
  { header: vendorHeader.Phone1,             key: vendorHeader.Phone1,                width: columnWidth },
  { header: vendorHeader.Phone2,             key: vendorHeader.Phone2,                width: columnWidth },
  { header: vendorHeader.Fax,                key: vendorHeader.Fax,                   width: columnWidth },
  { header: vendorHeader.Cellular,           key: vendorHeader.Cellular,              width: columnWidth },
  { header: vendorHeader.EmailAddress,       key: vendorHeader.EmailAddress,          width: columnWidth },
  { header: vendorHeader.Pager,              key: vendorHeader.Pager,                 width: columnWidth },
  { header: vendorHeader.Website,            key: vendorHeader.Website,               width: columnWidth },
  { header: vendorHeader.PayTermsGrpCode,    key: vendorHeader.PayTermsGrpCode,       width: columnWidth },
  { header: vendorHeader.CreditLimit,        key: vendorHeader.CreditLimit,           width: columnWidth },
  { header: vendorHeader.FreeText,           key: vendorHeader.FreeText,              width: columnWidth },
  { header: vendorHeader.SalesPersonCode,    key: vendorHeader.SalesPersonCode,       width: columnWidth },
  { header: vendorHeader.Currency,           key: vendorHeader.Currency,              width: columnWidth },
  { header: vendorHeader.SubjectToWithholdingTax,   key: vendorHeader.SubjectToWithholdingTax,  width: columnWidth },
  { header: vendorHeader.WTCode,             key: vendorHeader.WTCode,                width: columnWidth },
  { header: vendorHeader.VATNo,              key: vendorHeader.VATNo,                 width: columnWidth },
  { header: vendorHeader.CSTNo,              key: vendorHeader.CSTNo,                 width: columnWidth },
  { header: vendorHeader.PAN,                key: vendorHeader.PAN,                   width: columnWidth },
  { header: vendorHeader.ServiceTaxNo,       key: vendorHeader.ServiceTaxNo,          width: columnWidth },,
  { header: vendorHeader.ControlAccount,     key: vendorHeader.ControlAccount,        width: columnWidth },
  { header: vendorHeader.IsSubcontractor,    key: vendorHeader.IsSubcontractor,       width: columnWidth },
  { header: vendorHeader.U_OrigNum,          key: vendorHeader.U_OrigNum,             width: columnWidth },
  { header: vendorHeader["IsVendor"],              key: vendorHeader["IsVendor"],             width: columnWidth },
  { header: vendorHeader.CompanyName,          key: vendorHeader.CompanyName,             width: columnWidth }
  ];

  //OVPM1 WorkSheet
  var crd1Item = {
    ParentKey: "ParentKey",
    AddressType: "AddressType",
    AddressName: "AddressName",
    BuildingFloorRoom: "BuildingFloorRoom",
    Block: "Block",
    Street: "Street",
    City: "City",
    ZipCode: "ZipCode",
    State: "State",
    Country: "Country",
    ECCNo: "ECCNo",
    Range: "Range",
    Division: "Division",
    Commissionerate:"Commissionerate"
  };

  worksheet_crd1.columns = [
  { header: crd1Item.ParentKey,            key: crd1Item.ParentKey,           width: columnWidth },
  { header: crd1Item.AddressType,          key: crd1Item.AddressType,         width: columnWidth },
  { header: crd1Item.AddressName,          key: crd1Item.AddressName,         width: columnWidth },
  { header: crd1Item.BuildingFloorRoom,    key: crd1Item.BuildingFloorRoom,   width: columnWidth },
  { header: crd1Item.Block,                key: crd1Item.Block,               width: columnWidth },
  { header: crd1Item.Street,               key: crd1Item.Street,              width: columnWidth },
  { header: crd1Item.City,                 key: crd1Item.City,                width: columnWidth },
  { header: crd1Item.ZipCode,              key: crd1Item.ZipCode,             width: columnWidth },
  { header: crd1Item.State,                key: crd1Item.State,               width: columnWidth },
  { header: crd1Item.Country,              key: crd1Item.Country,             width: columnWidth },
  { header: crd1Item.ECCNo,                key: crd1Item.ECCNo,               width: columnWidth },
  { header: crd1Item.Range,                key: crd1Item.Range,               width: columnWidth },
  { header: crd1Item.Division,             key: crd1Item.Division,            width: columnWidth },
  { header: crd1Item.Commissionerate,      key: crd1Item.Commissionerate,     width: columnWidth }
  ];  

  qbo.findTerms('', function(err1, terms) {
      jsonObjPay = terms;
  });

  qbo.findVendors([
    {field: 'limit', value: 300}
    ], function(err, Vendors) {
      jsonObj = Vendors;
      var key = 'Vendor';
      var vendors = {"":""};
      vendors =  jsonObj.QueryResponse[key];

      var docnum = 1000;  
    //loop through each Payment 
      for (var i = 0 ; i < vendors.length; i++) {
        //dynamically increment
          docnum += 1;
        //pre-initialise field details if condition fails
          var CardName = "",
              Phone1 = "",
              EmailAddress = "",
              Website = "",
              PayTermsGrpCode = "",
              Notes = "",
              BuildingFloorRoom = "",
              Block = "",
              Street = "",
              City = "",
              State = "",
              U_OrigNum = "",
              isVendor = " ",
              CompanyName = "",
              ZipCode = "";
          //get card code
            if(vendors[i].hasOwnProperty("DisplayName") ){ 
              CardName = vendors[i].DisplayName;
            }
          //get phone Number
            if(vendors[i].hasOwnProperty("PrimaryPhone") && vendors[i].PrimaryPhone.hasOwnProperty("FreeFormNumber")){
              Phone1 = vendors[i].PrimaryPhone.FreeFormNumber;
            }

          //get email Address
            if(vendors[i].hasOwnProperty("PrimaryEmailAddr") && vendors[i].PrimaryEmailAddr.hasOwnProperty("Address")){
              EmailAddress = vendors[i].PrimaryEmailAddr.Address;
            }

          //get website
            if(vendors[i].hasOwnProperty("WebAddr") && vendors[i].WebAddr.hasOwnProperty("URI")){
              Website = vendors[i].WebAddr.URI;
            }

          //SalesTermRef
          if(vendors[i].hasOwnProperty("TermRef") && vendors[i].TermRef.hasOwnProperty("value")){
            var termMethod = jsonObjPay.QueryResponse["Term"];
            //loop through PaymentMethod Object to fetch paytype
              for(var k = termMethod.length - 1; k >= 0; k-- ){ 
                if(termMethod[k].Id == vendors[i].TermRef.value){
                  PayTermsGrpCode = termMethod[k].Name;
                }                 
              }
          }
          //console.log(PayTermsGrpCode);
          
          
          //u_OrigNum
          if(vendors[i].hasOwnProperty("Id")){
            U_OrigNum = vendors[i].Id;
          }
          //Free text 
          // if(vendors[i].hasOwnProperty("Notes")){
          //   Notes = vendors[i].Notes;
          // }

          

          if(vendors[i].hasOwnProperty("CompanyName")){
            CompanyName = vendors[i].CompanyName;
          }

          if(vendors[i].hasOwnProperty("Vendor1099")){
            isVendor = vendors[i].Vendor1099;
          }
         console.log(vendors[i].Vendor1099);
        //fillthe header worksheet
          worksheet_ocrd.addRow({
            CardCode : docnum,
            CardName : CardName,
            CardForeignName : " ",
            CardType : "cVendor",
            GroupCode : " ",
            Phone1 : Phone1,
            Phone2 : " ",
            Fax : " ",
            Cellular : " ",
            EmailAddress : EmailAddress,
            Pager : " ",
            Website : Website,
            PayTermsGrpCode : PayTermsGrpCode,
            reditLimit : " ",
            FreeText : " ",
            SalesPersonCode :" ",
            Currency : "USD",
            SubjectToWithholdingTax : "tNO",
            WTCode : " ",
            VATNo : "",
            CSTNo : "",
            PAN : " ",
            ServiceTaxNo : " ",
            ControlAccount : " ",
            IsSubcontractor : " ",
            U_OrigNum: U_OrigNum ,
            "IsVendor" : "",
            CompanyName: CompanyName      
          }).commit;

        //Address Details
          if(vendors[i].hasOwnProperty("BillAddr")){
            if(vendors[i].BillAddr.hasOwnProperty("Line1")){
              BuildingFloorRoom = vendors[i].BillAddr.Line1;
            }
            if(vendors[i].BillAddr.hasOwnProperty("Line2")){
              Block = vendors[i].BillAddr.Line2;
            }
            if(vendors[i].BillAddr.hasOwnProperty("Line3")){
              Street = vendors[i].BillAddr.Line3;
            }
            if(vendors[i].BillAddr.hasOwnProperty("City")){
              City = vendors[i].BillAddr.City;
            }
            if(vendors[i].BillAddr.hasOwnProperty("PostalCode")){
              ZipCode = vendors[i].BillAddr.PostalCode;
            }
            if(vendors[i].BillAddr.hasOwnProperty("CountrySubDivisionCode")){
              State = vendors[i].BillAddr.CountrySubDivisionCode;
            }
          }        
          //console.log(BuildingFloorRoom, Block, Street, City, ZipCode, State, i);
         worksheet_crd1.addRow({
          ParentKey: docnum,
          AddressType: "",
          AddressName: "",
          BuildingFloorRoom: BuildingFloorRoom,
          Block: Block,
          Street: Street,
          City: City,
          ZipCode: ZipCode,
          State: State,
          Country: "USA",
          ECCNo: "",
          Range: "",
          Division: "",
          Commissionerate:""
         }).commit; 
      } 
    worksheet_ocrd.commit();
    worksheet_crd1.commit();
    workbook.commit(); 
  });
});

module.exports = router;