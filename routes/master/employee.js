 
var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
http          = require('http'),
port          = process.env.PORT || 3000,
request       = require('request'),
qs            = require('querystring'),
util          = require('util');
var path          = require('path');
var router = express.Router(),
QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
tmpEmployeeInfo = {'key':'value'},
jsonObj = {'key':'value'},
jsonObjPay = {'key':'value'},
jsonObjAcc = {'key':'value'},
columnWidth = 30;

router.post("/getEmployee", function(req, res) {

  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet_ohem = workbook.addWorksheet("OHEM");
  var worksheet_oitm = workbook.addWorksheet("OITM");
 
  //OVPM WorkSheet
  var employeeHeader = {
    EmployeeID : "EmployeeID",
    LastName  : "LastName",
    FirstName : "FirstName",
    MiddleName  : "MiddleName",
    MobilePhone : "MobilePhone",
    eMail : "eMail",
    HomePhone : "HomePhone",
    StartDate : "StartDate",
    Gender : "Gender", 
    HomeCity: "HomeCity",
    HomeCountry : "HomeCountry",
    HomeState : "HomeState",
    DateOfBirth : "DateOfBirth",
    HomeStreet : "HomeStreet", 
    HomeZipCode : "HomeZipCode",
    ExternalID  : "ExternalID",
    JobTitle : "JobTitle", 
    Department : "Department", 
    Branch : "Branch",
    Manager : "Manager",
    ApplicationUserID : "ApplicationUserID",
    SalesPersonCode: "SalesPersonCode", 
    OfficePhone : "OfficePhone",
    OfficeExtension : "OfficeExtension",
    Salary  : "Salary",
    BankCode : "BankCode", 
    BankBranch: "BankBranch",
    BankBranchNum : "BankBranchNum",
    BankAccount : "BankAccount",
    HomeBlock : "HomeBlock",
    CountryOfBirth  : "CountryOfBirth",
    MartialStatus : "MartialStatus",
    NumOfChildren : "NumOfChildren",
    IdNumber: "IdNumber",
    SSN : "SSN",
    ReleasedDate: "ReleasedDate"
  };


  worksheet_ohem.columns = [
  { header: employeeHeader.EmployeeID,           key: employeeHeader.EmployeeID,              width: columnWidth },
  { header: employeeHeader.LastName,           key: employeeHeader.LastName,                 width: columnWidth },
  { header: employeeHeader.FirstName,        key: employeeHeader.FirstName,                  width: columnWidth },
  { header: employeeHeader.MiddleName,           key: employeeHeader.MiddleName,              width: columnWidth },
  { header: employeeHeader.MobilePhone,          key: employeeHeader.MobilePhone,             width: columnWidth },
  { header: employeeHeader.eMail,             key: employeeHeader.eMail,                       width: columnWidth },
  { header: employeeHeader.HomePhone,             key: employeeHeader.HomePhone,                width: columnWidth },
  { header: employeeHeader.StartDate,                key: employeeHeader.StartDate,                   width: columnWidth },
  { header: employeeHeader.Gender,           key: employeeHeader.Gender,                        width: columnWidth },
  { header: employeeHeader.HomeCity,        key: employeeHeader.HomeCity,                       width: columnWidth },
  { header: employeeHeader.HomeCountry,              key: employeeHeader.HomeCountry,                 width: columnWidth },
  { header: employeeHeader.HomeState,            key: employeeHeader.HomeState,                      width: columnWidth },
  { header: employeeHeader.DateOfBirth,      key: employeeHeader.DateOfBirth,               width: columnWidth },
  { header: employeeHeader.HomeStreet,        key: employeeHeader.HomeStreet,                 width: columnWidth },
  { header: employeeHeader.HomeZipCode,           key: employeeHeader.HomeZipCode,              width: columnWidth },
  { header: employeeHeader.ExternalID,       key: employeeHeader.ExternalID,                  width: columnWidth },
  { header: employeeHeader.JobTitle,           key: employeeHeader.JobTitle,                   width: columnWidth },
  { header: employeeHeader.Department,        key: employeeHeader.Department,                 width: columnWidth },
  { header: employeeHeader.Branch,             key: employeeHeader.Branch,                     width: columnWidth },
  { header: employeeHeader.Manager,              key: employeeHeader.Manager,               width: columnWidth },
  { header: employeeHeader.ApplicationUserID,    key: employeeHeader.ApplicationUserID,     width: columnWidth },
  { header: employeeHeader.SalesPersonCode,       key: employeeHeader.SalesPersonCode,     width: columnWidth },
  { header: employeeHeader.OfficePhone,         key: employeeHeader.OfficePhone,                width: columnWidth },
  { header: employeeHeader.OfficeExtension,     key: employeeHeader.OfficeExtension,               width: columnWidth },
  { header: employeeHeader.Salary,             key: employeeHeader.Salary,                     width: columnWidth },
  { header: employeeHeader.BankCode,          key: employeeHeader.BankCode,                 width: columnWidth },
  { header: employeeHeader.BankBranch,                key: employeeHeader.BankBranch,       width: columnWidth },
  { header: employeeHeader.BankBranchNum,        key: employeeHeader.BankBranchNum,          width: columnWidth },
  { header: employeeHeader.BankAccount,        key: employeeHeader.BankAccount,               width: columnWidth },
  { header: employeeHeader.HomeBlock,          key: employeeHeader.HomeBlock,               width: columnWidth },
  { header: employeeHeader.CountryOfBirth,          key: employeeHeader.CountryOfBirth,      width: columnWidth },
  { header: employeeHeader.MartialStatus,          key: employeeHeader.MartialStatus,        width: columnWidth },
  { header: employeeHeader.NumOfChildren,                key: employeeHeader.NumOfChildren,       width: columnWidth },
  { header: employeeHeader.IdNumber,             key: employeeHeader.IdNumber,                    width: columnWidth },
  { header: employeeHeader.SSN,             key: employeeHeader.SSN,                             width: columnWidth },
  { header: employeeHeader.ReleasedDate,      key: employeeHeader.ReleasedDate,            width: columnWidth }
  ];

  //OITM WorkSheet
  var ItemHeader = {
    ItemCode : "ItemCode",
    ItemName: "ItemName",
    ItemGroup: "ItemGroup",
    SubGroup: "SubGroup",
    PurchaseItem: "PurchaseItem",
    SalesItem: "SalesItem",
    InventoryItem: "InventoryItem",
    Mainsupplier: "Mainsupplier",
    DesiredInventory:"DesiredInventory",
    MinInventory:"MinInventory",
    ManageSerialNumbers:"ManageSerialNumbers",
    ManageBatchNumbers:"ManageBatchNumbers",
    SalesUnit:"SalesUnit",
    PurchaseUnit:"PurchaseUnit",
    InventoryUOM: "InventoryUOM",
    ProcurementMethod:"ProcurementMethod",
    LeadTime:"LeadTime",
    MinOrderQuantity:"MinOrderQuantity",
    Excisable:"Excisable",
    MaterialType: "MaterialType",
    ChapterID:"ChapterID",
    PerformInspection:"PerformInspection",
    BatchInspection:"BatchInspection"
  };
  
  worksheet_oitm.columns = [
  { header: ItemHeader.ItemCode,           key: ItemHeader.ItemCode,          width: columnWidth },
  { header: ItemHeader.ItemName,           key: ItemHeader.ItemName,          width: columnWidth },
  { header: ItemHeader.ItemGroup,          key: ItemHeader.ItemGroup,         width: columnWidth },
  { header: ItemHeader.SubGroup,           key: ItemHeader.SubGroup,          width: columnWidth },
  { header: ItemHeader.PurchaseItem,       key: ItemHeader.PurchaseItem,      width: columnWidth },
  { header: ItemHeader.SalesItem,          key: ItemHeader.SalesItem,         width: columnWidth },
  { header: ItemHeader.InventoryItem,      key: ItemHeader.InventoryItem,     width: columnWidth },
  { header: ItemHeader.Mainsupplier,       key: ItemHeader.Mainsupplier,      width: columnWidth },
  { header: ItemHeader.DesiredInventory,   key: ItemHeader.DesiredInventory,  width: columnWidth },
  { header: ItemHeader.MinInventory,       key: ItemHeader.MinInventory,      width: columnWidth },
  { header: ItemHeader.ManageSerialNumbers,key: ItemHeader.ManageSerialNumbers,width: columnWidth },
  { header: ItemHeader.ManageBatchNumbers, key: ItemHeader.ManageBatchNumbers,width: columnWidth },
  { header: ItemHeader.SalesUnit,          key: ItemHeader.SalesUnit,         width: columnWidth },
  { header: ItemHeader.PurchaseUnit,       key: ItemHeader.PurchaseUnit,      width: columnWidth },
  { header: ItemHeader.InventoryUOM,       key: ItemHeader.InventoryUOM,      width: columnWidth },
  { header: ItemHeader.ProcurementMethod,  key: ItemHeader.ProcurementMethod, width: columnWidth },
  { header: ItemHeader.LeadTime,           key: ItemHeader.LeadTime,          width: columnWidth },
  { header: ItemHeader.MinOrderQuantity,   key: ItemHeader.MinOrderQuantity,  width: columnWidth },
  { header: ItemHeader.Excisable,          key: ItemHeader.Excisable,         width: columnWidth },
  { header: ItemHeader.MaterialType,       key: ItemHeader.MaterialType,      width: columnWidth },
  { header: ItemHeader.ChapterID,          key: ItemHeader.ChapterID,         width: columnWidth },
  { header: ItemHeader.PerformInspection,  key: ItemHeader.PerformInspection, width: columnWidth },
  { header: ItemHeader.BatchInspection,    key: ItemHeader.BatchInspection,   width: columnWidth },
  ]; 

  qbo.findPaymentMethods('', function(err1, paymentmethod) {
      jsonObjPay = paymentmethod;
  });

  qbo.findEmployees([
    {field: 'limit', value: 650}
    ], function(err, Employees) {
      jsonObj = Employees;
      var key = 'Employee';
      var employees = {"":""};
      employees =  jsonObj.QueryResponse[key];

      var docnum = 1000;      

    //loop through each Payment 
      for (var i = 0 ; i < employees.length; i++) {
        //dynamically increment
          docnum += 1;
        //pre-initialise field details if condition fails
          var GivenName = "",
              FamilyName = "",
              EmailAddress = "",
              Phone1 = "",
              StartDate = "",
              ReleaseDate = "",
              BirthDate = "",
              Gender = "",
              SSN = "",
              HomeStreet = "",
              City = "",
              State = "",
              ZipCode = "";
          
          //get details
            if(employees[i].hasOwnProperty("DisplayName")){
              var name = employees[i].DisplayName.split(" ");
              GivenName = name[0];
              if(name[1] == ""){
                FamilyName = name[2];
              }
              else{
                FamilyName = name[1];
              }
            }           
            if(employees[i].hasOwnProperty("GivenName") ){ 
              GivenName = employees[i].GivenName;
            } 
            if(employees[i].hasOwnProperty("FamilyName") ){ 
              FamilyName = employees[i].FamilyName;
            } 
            if(employees[i].hasOwnProperty("Gender") ){ 
              Gender = employees[i].Gender;
            }

          //get phone Number
            if(employees[i].hasOwnProperty("PrimaryPhone") && employees[i].PrimaryPhone.hasOwnProperty("FreeFormNumber")){
              Phone1 = employees[i].PrimaryPhone.FreeFormNumber;
            }

          //get email Address
            if(employees[i].hasOwnProperty("PrimaryEmailAddr") && employees[i].PrimaryEmailAddr.hasOwnProperty("Adress")){
              EmailAddress = employees[i].PrimaryEmailAddr.Adress;
            }

          //get SSN
           if(employees[i].hasOwnProperty("SSN")){
              SSN = employees[i].SSN;
           }

          //Get Address
          if(employees[i].hasOwnProperty("PrimaryAddr")){
            if(employees[i].PrimaryAddr.hasOwnProperty("City")){
              City = employees[i].PrimaryAddr.City;
            }
            if(employees[i].PrimaryAddr.hasOwnProperty("CountrySubDivisionCode")){
              State = employees[i].PrimaryAddr.CountrySubDivisionCode;
            }
            if(employees[i].PrimaryAddr.hasOwnProperty("PostalCode")){
              ZipCode = employees[i].PrimaryAddr.PostalCode;
            }
            if(employees[i].PrimaryAddr.hasOwnProperty("Line1")){
              HomeStreet = employees[i].PrimaryAddr.Line1;
            }
            if(employees[i].PrimaryAddr.hasOwnProperty("Line2")){
              HomeStreet += employees[i].PrimaryAddr.Line2;
            }
            if(employees[i].PrimaryAddr.hasOwnProperty("Line3")){
              HomeStreet += employees[i].PrimaryAddr.Line3;
            }

          }
          console.log(i ,HomeStreet);

        //get dates
          if(employees[i].hasOwnProperty("MetaData") && employees[i].MetaData.hasOwnProperty("CreateTime")){
            StartDate = employees[i].MetaData.CreateTime;
          }

          if(employees[i].hasOwnProperty("BirthDate") && employees[i].BirthDate.hasOwnProperty("date")){
            BirthDate = employees[i].BirthDate.date;
          }

          if(employees[i].hasOwnProperty("ReleasedDate") && employees[i].ReleasedDate.hasOwnProperty("date")){
            ReleaseDate = employees[i].ReleasedDate.date;
          }


        //fillthe header worksheet
          worksheet_ohem.addRow({
            EmployeeID : docnum,
            LastName  : FamilyName,
            FirstName : GivenName,
            MiddleName  : "",
            MobilePhone : Phone1,
            eMail : EmailAddress,
            HomePhone : "",
            StartDate : StartDate,
            Gender : Gender, 
            HomeCity: City,
            HomeCountry : "USA",
            HomeState : State,
            DateOfBirth : BirthDate,
            HomeStreet : HomeStreet, 
            HomeZipCode : ZipCode,
            ExternalID  : employees[i].Id,
            JobTitle : "", 
            Department : "", 
            Branch : "",
            Manager : "",
            ApplicationUserID : "",
            SalesPersonCode: "", 
            OfficePhone : "",
            OfficeExtension : "",
            Salary  : "",
            BankCode : "", 
            BankBranch: "",
            BankBranchNum : "",
            BankAccount : "",
            HomeBlock : "",
            CountryOfBirth  : "",
            MartialStatus : "",
            NumOfChildren : "",
            IdNumber: "",
            SSN : SSN,
            ReleasedDate: ReleaseDate            
          }).commit;

         //item Name

          console.log(docnum);
         worksheet_oitm.addRow({
            ItemCode : docnum,
            ItemName: GivenName+" "+FamilyName,
            ItemGroup: "",
            SubGroup: "",
            PurchaseItem: "Yes",
            SalesItem: "Yes",
            InventoryItem: "No",
            Mainsupplier: "",
            DesiredInventory:"",
            MinInventory:"",
            ManageSerialNumbers:"",
            ManageBatchNumbers:"",
            SalesUnit:"Hours",
            PurchaseUnit:"Hours",
            InventoryUOM: "",
            ProcurementMethod:"",
            LeadTime:"",
            MinOrderQuantity:"",
            Excisable:"",
            MaterialType: "",
            ChapterID:"",
            PerformInspection:"",
            BatchInspection:""
         }).commit; 
      } 
    worksheet_ohem.commit();
    worksheet_oitm.commit();
    workbook.commit(); 
  });
});

module.exports = router; 