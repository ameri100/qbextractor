
var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
http          = require('http'),
port          = process.env.PORT || 3000,
request       = require('request'),
qs            = require('querystring'),
util          = require('util');
var path          = require('path');
var router = express.Router(),
QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
tmpEmployeeInfo = {'key':'value'},
jsonObj = {'key':'value'},
jsonObjPay = {'key':'value'},
jsonObjAcc = {'key':'value'},
columnWidth = 30;

router.post("/getCustomer", function(req, res) {

  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet_OCRD = workbook.addWorksheet("OCRD");
  var worksheet_CRD1 = workbook.addWorksheet("CRD1");
 
  //OVPM WorkSheet
  var customerHeader = {
    CardCode : "CardCode",
    CardName: "CardName",
    CardForeignName: "CardForeignName",
    CardType: "CardType",
    GroupCode: "GroupCode",
    Phone1: "Phone1",
    Phone2: "Phone2",
    Fax: "Fax",
    Cellular:"Cellular",
    EmailAddress:"EmailAddress",
    Pager:"Pager",
    Website:"Website",
    PayTermsGrpCode:"PayTermsGrpCode",
    CreditLimit:"CreditLimit",
    FreeText: "FreeText",
    SalesPersonCode:"SalesPersonCode",
    Currency:"Currency",
    SubjectToWithholdingTax:"SubjectToWithholdingTax",
    WTCode:"WTCode",
    VATNo: "VATNo",
    CSTNo:"CSTNo",
    PAN:"PAN",
    ServiceTaxNo:"ServiceTaxNo",
    ControlAccount: "ControlAccount",
    IsSubcontractor:"IsSubcontractor",
    U_OrigNum:"U_OrigNum"
  };

  worksheet_OCRD.columns = [
  { header: customerHeader.CardCode,           key: customerHeader.CardCode,              width: columnWidth },
  { header: customerHeader.CardName,           key: customerHeader.CardName,              width: columnWidth },
  { header: customerHeader.CardForeignName,    key: customerHeader.CardForeignName,       width: columnWidth },
  { header: customerHeader.CardType,           key: customerHeader.CardType,              width: columnWidth },
  { header: customerHeader.GroupCode,          key: customerHeader.GroupCode,             width: columnWidth },
  { header: customerHeader.Phone1,             key: customerHeader.Phone1,                width: columnWidth },
  { header: customerHeader.Phone2,             key: customerHeader.Phone2,                width: columnWidth },
  { header: customerHeader.Fax,                key: customerHeader.Fax,                   width: columnWidth },
  { header: customerHeader.Cellular,           key: customerHeader.Cellular,              width: columnWidth },
  { header: customerHeader.EmailAddress,       key: customerHeader.EmailAddress,          width: columnWidth },
  { header: customerHeader.Pager,              key: customerHeader.Pager,                 width: columnWidth },
  { header: customerHeader.Website,            key: customerHeader.Website,               width: columnWidth },
  { header: customerHeader.PayTermsGrpCode,    key: customerHeader.PayTermsGrpCode,       width: columnWidth },
  { header: customerHeader.CreditLimit,        key: customerHeader.CreditLimit,           width: columnWidth },
  { header: customerHeader.FreeText,           key: customerHeader.FreeText,              width: columnWidth },
  { header: customerHeader.SalesPersonCode,    key: customerHeader.SalesPersonCode,       width: columnWidth },
  { header: customerHeader.Currency,           key: customerHeader.Currency,              width: columnWidth },
  { header: customerHeader.SubjectToWithholdingTax,            key: customerHeader.SubjectToWithholdingTax,               width: columnWidth },
  { header: customerHeader.WTCode,             key: customerHeader.WTCode,                width: columnWidth },
  { header: customerHeader.VATNo,              key: customerHeader.VATNo,                 width: columnWidth },
  { header: customerHeader.CSTNo,              key: customerHeader.CSTNo,                 width: columnWidth },
  { header: customerHeader.PAN,                key: customerHeader.PAN,                   width: columnWidth },
  { header: customerHeader.ServiceTaxNo,       key: customerHeader.ServiceTaxNo,          width: columnWidth },
  { header: customerHeader.ControlAccount,     key: customerHeader.ControlAccount,        width: columnWidth },
  { header: customerHeader.IsSubcontractor,    key: customerHeader.IsSubcontractor,       width: columnWidth },
  { header: customerHeader.U_OrigNum,          key: customerHeader.U_OrigNum,             width: columnWidth }
  ];

  //OVPM1 WorkSheet
  var crd1Item = {
    ParentKey: "ParentKey",
    AddressType: "AddressType",
    AddressName: "AddressName",
    BuildingFloorRoom: "BuildingFloorRoom",
    Block: "Block",
    Street: "Street",
    City: "City",
    ZipCode: "ZipCode",
    State: "State",
    Country: "Country",
    ECCNo: "ECCNo",
    Range: "Range",
    Division: "Division",
    Commissionerate:"Commissionerate"
  };

  worksheet_CRD1.columns = [
  { header: crd1Item.ParentKey,            key: crd1Item.ParentKey,           width: columnWidth },
  { header: crd1Item.AddressType,          key: crd1Item.AddressType,         width: columnWidth },
  { header: crd1Item.AddressName,          key: crd1Item.AddressName,         width: columnWidth },
  { header: crd1Item.BuildingFloorRoom,    key: crd1Item.BuildingFloorRoom,   width: columnWidth },
  { header: crd1Item.Block,                key: crd1Item.Block,               width: columnWidth },
  { header: crd1Item.Street,               key: crd1Item.Street,              width: columnWidth },
  { header: crd1Item.City,                 key: crd1Item.City,                width: columnWidth },
  { header: crd1Item.ZipCode,              key: crd1Item.ZipCode,             width: columnWidth },
  { header: crd1Item.State,                key: crd1Item.State,               width: columnWidth },
  { header: crd1Item.Country,              key: crd1Item.Country,             width: columnWidth },
  { header: crd1Item.ECCNo,                key: crd1Item.ECCNo,               width: columnWidth },
  { header: crd1Item.Range,                key: crd1Item.Range,               width: columnWidth },
  { header: crd1Item.Division,             key: crd1Item.Division,            width: columnWidth },
  { header: crd1Item.Commissionerate,      key: crd1Item.Commissionerate,     width: columnWidth }
  ];  

  qbo.findTerms('', function(err1, term) {
      jsonObjPay = term;
  });

  qbo.findCustomers('', function(err, Customers) {
      jsonObj = Customers;
      var key = 'Customer';
      var customers = {"":""};
      customers =  jsonObj.QueryResponse[key];

      var docnum = 1000;      

    //loop through each Payment 
      for (var i = 0 ; i < customers.length; i++) {
        //dynamically increment
          docnum += 1;
        //pre-initialise field details if condition fails
          var CardName = "",
              Phone1 = "",
              EmailAddress = "",
              Website = "",
              PayTermsGrpCode = "",
              Notes = "",
              BuildingFloorRoom = "",
              Block = "",
              Street = "",
              City = "",
              State = "",
              ZipCode = "";
          
          //get card code
            if(customers[i].hasOwnProperty("CompanyName") ){ 
              CardName = customers[i].CompanyName;
            } 
            else if(customers[i].hasOwnProperty("DisplayName")){
              CardName = customers[i].DisplayName;
            }

          //get phone Number
            if(customers[i].hasOwnProperty("PrimaryPhone") && customers[i].PrimaryPhone.hasOwnProperty("FreeFormNumber")){
              Phone1 = customers[i].PrimaryPhone.FreeFormNumber;
            }

          //get email Address
            if(customers[i].hasOwnProperty("PrimaryEmailAddr") && customers[i].PrimaryEmailAddr.hasOwnProperty("Address")){
              EmailAddress = customers[i].PrimaryEmailAddr.Address;
            }

          //get website
            if(customers[i].hasOwnProperty("WebAddr") && customers[i].WebAddr.hasOwnProperty("URI")){
              Website = customers[i].WebAddr.URI;
            }
          
          //SalesTermRef
          var paymentmethods = jsonObjPay.QueryResponse["Term"];
        //loop through PaymentMethod Object to fetch paytype
          for(var k = paymentmethods.length - 1; k >= 0; k-- ){  
            if(customers[i].hasOwnProperty("SalesTermRef") && customers[i].SalesTermRef.hasOwnProperty("value")){
              if(paymentmethods[k].Id == customers[i].SalesTermRef.value){
                PayTermsGrpCode = paymentmethods[k].Name;
              }                 
            }
          }

          //Free text 
          if(customers[i].hasOwnProperty("Notes")){
           // Notes = customers[i].Notes;
          }

          console.log(docnum ,"=" , CardName);
        //fillthe header worksheet
          worksheet_OCRD.addRow({
            CardCode : docnum,
            CardName : CardName,
            CardForeignName : " ",
            CardType : "cCustomer",
            GroupCode : " ",
            Phone1 : Phone1,
            Phone2 : " ",
            Fax : " ",
            Cellular : " ",
            EmailAddress : EmailAddress,
            Pager : " ",
            Website : Website,
            PayTermsGrpCode : PayTermsGrpCode,
            CreditLimit : " ",
            FreeText : Notes,
            SalesPersonCode : " ",
            Currency : "USD",
            SubjectToWithholdingTax : "tNO",
            WTCode : " ",
            VATNo : " ",
            CSTNo : " ",
            PAN : " ",
            ServiceTaxNo : " ",
            ControlAccount : " ",
            IsSubcontractor : " ",
            U_OrigNum:customers[i].Id              
          }).commit;

        //Address Details
          if(customers[i].hasOwnProperty("BillAddr")){
            if(customers[i].BillAddr.hasOwnProperty("Line1")){
              BuildingFloorRoom = customers[i].BillAddr.Line1;
            }
            if(customers[i].BillAddr.hasOwnProperty("Line2")){
              Block = customers[i].BillAddr.Line2;
            }
            if(customers[i].BillAddr.hasOwnProperty("Line3")){
              Street = customers[i].BillAddr.Line3;
            }
            if(customers[i].BillAddr.hasOwnProperty("City")){
              City = customers[i].BillAddr.City;
            }
            if(customers[i].BillAddr.hasOwnProperty("PostalCode")){
              ZipCode = customers[i].BillAddr.PostalCode;
            }
            if(customers[i].BillAddr.hasOwnProperty("CountrySubDivisionCode")){
              State = customers[i].BillAddr.CountrySubDivisionCode;
            }
          }        

         worksheet_CRD1.addRow({
          ParentKey : docnum,
          AddressType : " ",
          AddressName : " ",
          BuildingFloorRoom : BuildingFloorRoom,
          Block : Block,
          Street : Street,
          City : City,
          ZipCode : ZipCode,
          State : State,
          Country : "USA",
          ECCNo : " ",
          Range : " ",
          Division : " ",
          Commissionerate : " "
         }).commit; 
      } 
    worksheet_OCRD.commit();
    worksheet_CRD1.commit();
    workbook.commit(); 
  });
});

module.exports = router;