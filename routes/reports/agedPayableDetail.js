var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
http          = require('http'),
port          = process.env.PORT || 3000,
request       = require('request'),
qs            = require('querystring'),
util          = require('util');
var path          = require('path');
var router = express.Router(),
QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
jsonObj = {'key':'value'},
columnWidth = 30;

router.post("/getAgedPayables", function(req, res) {
  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet_report = workbook.addWorksheet("AgedPayable");

  var reportHeader = {
    Date: "Date",
    TransactionTypeName: "TransactionTypeName",
    TransactionTypeId: "TransactionTypeId",
    Num: "Num",
    Vendor: "Vendor",
    VendorId: "VendorId",
    Business: "Business",
    BusinessId: "BusinessId",
    DueDate: "DueDate",
    PastDue:"PastDue",
    Amount: "Amount",
    OpenBalance:"OpenBalance"
  };
  worksheet_report.columns = [
  { header: reportHeader.Date,          key: reportHeader.Date,         width: columnWidth },
    { header: reportHeader.TransactionTypeName,   key: reportHeader.TransactionTypeName,  width: columnWidth },
    { header: reportHeader.TransactionTypeId,   key: reportHeader.TransactionTypeId,  width: columnWidth },
    { header: reportHeader.Num,            key: reportHeader.Num,           width: columnWidth },
    { header: reportHeader.Vendor,       key: reportHeader.Vendor,      width: columnWidth },
    { header: reportHeader.VendorId,       key: reportHeader.VendorId,      width: columnWidth },
    { header: reportHeader.Business,      key: reportHeader.Business,     width: columnWidth },
    { header: reportHeader.BusinessId,      key: reportHeader.BusinessId,     width: columnWidth },
    { header: reportHeader.DueDate,         key: reportHeader.DueDate,        width: columnWidth },
    { header: reportHeader.PastDue,         key: reportHeader.PastDue,        width: columnWidth },
    { header: reportHeader.Amount,         key: reportHeader.Amount,        width: columnWidth },
    { header: reportHeader.OpenBalance,          key: reportHeader.OpenBalance,         width: columnWidth },];

  qbo.reportAgedPayableDetail({report_date: '2016-12-31'}, function(err1, AgedPayable) {   
    for( var j=0; j<AgedPayable.Rows.Row.length-1; j++){  // -1 because last entity will be summary which is not needed

      for(var i=0;i<AgedPayable.Rows.Row[j].Rows.Row.length; i++){
        // console.log(AgedPayable.Rows.Row[j].Summary.ColData[0].value)
          worksheet_report.addRow({
                Date: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[0].value,
        		    TransactionTypeName: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[1].value,
        		    TransactionTypeId: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[1].id,
        		    Num: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[2].value,
        		    Vendor: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[3].value,
        		    VendorId: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[3].id,
        		    Business: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[4].value,
            		BusinessId: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[4].id,
        		    DueDate: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[5].value,
                PastDue: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[6].value,
        		    Amount: AgedPayable.Rows.Row[j].Rows.Row[i].ColData[7].value,
        		    OpenBalance:AgedPayable.Rows.Row[j].Rows.Row[i].ColData[8].value
              }).commit;
        }
      }
      worksheet_report.commit();      
      workbook.commit();
  });
});

module.exports = router;