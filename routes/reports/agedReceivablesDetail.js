var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
http          = require('http'),
port          = process.env.PORT || 3000,
request       = require('request'),
qs            = require('querystring'),
util          = require('util');
var path          = require('path');
var router = express.Router(),
QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
jsonObj = {'key':'value'},
columnWidth = 30;

router.post("/getAgedReceivables", function(req, res) {
  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet_report = workbook.addWorksheet("AgedReceivable");

  var reportHeader = {
  	Date: "Date",
    TransactionTypeName: "TransactionTypeName",
    TransactionTypeId: "TransactionTypeId",
    Num: "Num",
    Customer: "Customer",
    CustomerId: "CustomerId",
    Business: "Business",
    BusinessId: "BusinessId",
    DueDate: "DueDate",
    Amount: "Amount",
    OpenBalance:"OpenBalance"
  };
  worksheet_report.columns = [
  { header: reportHeader.Date,          key: reportHeader.Date,         width: columnWidth },
    { header: reportHeader.TransactionTypeName,   key: reportHeader.TransactionTypeName,  width: columnWidth },
    { header: reportHeader.TransactionTypeId,   key: reportHeader.TransactionTypeId,  width: columnWidth },
    { header: reportHeader.Num,            key: reportHeader.Num,           width: columnWidth },
    { header: reportHeader.Customer,       key: reportHeader.Customer,      width: columnWidth },
    { header: reportHeader.CustomerId,       key: reportHeader.CustomerId,      width: columnWidth },
    { header: reportHeader.Business,      key: reportHeader.Business,     width: columnWidth },
    { header: reportHeader.BusinessId,      key: reportHeader.BusinessId,     width: columnWidth },
    { header: reportHeader.DueDate,         key: reportHeader.DueDate,        width: columnWidth },
    { header: reportHeader.Amount,         key: reportHeader.Amount,        width: columnWidth },
    { header: reportHeader.OpenBalance,          key: reportHeader.OpenBalance,         width: columnWidth },];

  qbo.reportAgedReceivableDetail({report_date: '2016-12-31'}, function(err1, AgedReceivable) {
  	for( var j=0; j<AgedReceivable.Rows.Row.length-1; j++){  // -1 because last entity will be summary which is not needed
	  	for(var i=0;i<AgedReceivable.Rows.Row[j].Rows.Row.length; i++){
	      worksheet_report.addRow({
	            Date: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[0].value,
			    TransactionTypeName: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[1].value,
			    TransactionTypeId: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[1].id,
			    Num: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[2].value,
			    Customer: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[3].value,
			    CustomerId: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[3].id,
			    Business: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[4].value,
	    		BusinessId: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[4].id,
			    DueDate: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[5].value,
			    Amount: AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[6].value,
			    OpenBalance:AgedReceivable.Rows.Row[j].Rows.Row[i].ColData[7].value
	          }).commit;
	      }
	  }
      worksheet_report.commit();      
      workbook.commit();
  });
});

module.exports = router;