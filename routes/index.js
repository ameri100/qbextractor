var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
    http          = require('http'),
    port          = process.env.PORT || 3000,
    request       = require('request'),
    qs            = require('querystring'),
    util          = require('util');
var path          = require('path');
var router = express.Router(),
  QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
    tmpEmployeeInfo = {'key':'value'},
    jsonObj = {'key':'value'},
    columnWidth = 30;

// INSERT YOUR CONSUMER_KEY AND CONSUMER_SECRET HERE

// sandbox informations, Development Keys
// var consumerKey    = 'qyprdsVrf01EAK5YvuXlCajFO4zAXe',
// consumerSecret = 'UcqoiV8DR4eK7E25IfV7DOG3NwmsKASq1zPKJrzI'
   // realm_id = '193514298535987'; //Sandbox id


// running Quickbook information Production Keys

var consumerKey    = 'qyprdAct0kKAhNys4QM7edLsKGB6Ei',
consumerSecret = 'f2gNxZc1ueYTNqXgsumPC5OfLePM3CQRBGehm1Ta'

//      realm_id = '84705963H22';  // BellSoft version id
//     //realm_id = '123145740392962';  // trial version id
  


/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get('/',function(req,res){
  res.redirect('/start');
})

router.get('/start', function(req, res) {
  res.render('quickbook_login.ejs', {locals: {port:port, appCenter: QuickBooks.APP_CENTER_BASE}})
});

router.get('/customer/:id', function (req, res) {
 // console.log(req.session);
//  console.log(req.params.id);
  qbo.getCustomer(req.params.id, function(err, customer) {
 //   console.log(customer);
    res.render('customer.ejs', { locals: { customer: customer }})
  })
});

router.get('/employee/:id', function (req, res) {
//  console.log(req.session);
//  console.log(req.params.id);
  qbo.findEmployees('', function(err, employee) {
    var obj = {'key':'value'};
 console.log(
             /* define stringify */
            // JSON.stringify(employee)
 );
  //  console.log(employee);
    tmpEmployeeInfo = employee;
    res.render('employee.ejs', { locals: { employee: employee }})
  })
});

router.get('/reports/:id', function (req, res) {
 // console.log(req.session);
 // console.log(req.params.id);
  qbo.getReports(req.params.id, function(err, reports) {
  //  console.log(reports);
    local_reports = reports;
    res.render('reports.ejs', { locals: { reports: reports }})
    
  })
})

router.get('/user', function(req, res){ 
res.render('user.ejs',{user:"John Smith"}) 
 }); 


router.post("/getEmployee", function(req, res) {

  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet = workbook.addWorksheet("My Sheet");

  worksheet.columns = [
    { header: "EmployeeID", key: "employeeID", width: 10 },
    { header: "ExternalID", key: "ExtId", width: 32 },
    { header: "LastName", key: "L_Name", width: 10 },
    { header: "FirstName", key: "F_Name", width: 10 },
    { header: "MiddleName", key: "M_Name", width: 10 },
    { header: "Gender", key: "Gender", width: 10 },
    { header: "JobTitle", key: "JobTitle", width: 10 },
    { header: "Department", key: "Department", width: 10 },
    { header: "Branch", key: "Domain", width: 10 },
    { header: "Manager", key: "Manager", width: 10 },
    { header: "ApplicationUserID", key: "ApplicationUserID", width: 10 },
    { header: "SalesPersonCode", key: "SalesPersonCode", width: 10 },
    { header: "OfficePhone", key: "OfficePhone", width: 10 },
    { header: "OfficeExtension", key: "OfficeExtension", width: 10 },
    { header: "MobilePhone", key: "MobilePhone", width: 10 },
    { header: "HomePhone", key: "HomePhone", width: 10 },
    { header: "eMail", key: "eMail", width: 10 },
    { header: "StartDate", key: "StartDate", width: 10 },
    { header: "Salary", key: "Salary", width: 10 },
    { header: "BankCode", key: "BankCode", width: 10 }

  ];

var emp_key = 'Employee';
var employees = tmpEmployeeInfo.QueryResponse[emp_key];
//console.log('Length of Employee',employees.length);
//console.log('ID of Employee',employees[0]["Id"]);
for (var i = 0 ; i < employees.length ; i++) {
  
 // console.log('ID of Employee',employees[i]["Id"]);
  worksheet.addRow({
     employeeID: employees[i].Id,
      L_Name: employees[i].FamilyName,
      F_Name: employees[i].GivenName,
      Domain: employees[i].Domain,
     Gender: "Female"
  }).commit;

};
  
  worksheet.commit();
  workbook.commit();
});

router.post("/getfile", function(req, res) {

  var options = {
    useStyles: true,
    useSharedStrings: true
  };

  var workbook = new Excel.stream.xlsx.WorkbookWriter(options);

  //console.log(workbook);

  workbook.zip.pipe(res);

  var worksheet = workbook.addWorksheet("My Sheet");

  worksheet.columns = [
    { header: local_reports.Header.ReportName, key: "id", width: 10 },
    { header: "Name", key: "Sudhir", width: 32 },
    { header: "D.O.B.", key: "13 Dec 1991//testing", width: 10 }
  ];

  worksheet.addRow({
     id: 100,
     name: "name",
     DOB: "DOB"
  }).commit();

  worksheet.commit();
  workbook.commit();
});

router.get('/routes/requestToken', function(req, res) {
  var postBody = {
    url: QuickBooks.REQUEST_TOKEN_URL,
    oauth: {
      callback:        'http://localhost:' + port + '/routes/callback/',//'https://appcenter.intuit.com/Connect/Begin',
      consumer_key:    consumerKey,
      consumer_secret: consumerSecret
    }
  }

 // console.log('count: %s',postBody.request );
  request.post(postBody, function (e, r, data) {
    var requestToken = qs.parse(data)
    req.session.oauth_token_secret = requestToken.oauth_token_secret
    // console.log(requestToken);
    // console.log(req.session);
    // console.log(req);
    
    res.redirect(QuickBooks.APP_CENTER_URL + requestToken.oauth_token)
  })
})

router.get('/routes/callback', function(req, res) {
  var postBody = {

    url: QuickBooks.ACCESS_TOKEN_URL,
    oauth: {
      consumer_key:    consumerKey,
      consumer_secret: consumerSecret,
      token:           req.query.oauth_token,
      token_secret:    req.session.oauth_token_secret,
      verifier:        req.query.oauth_verifier,

      realmId:         req.query.realmId
      //req.query.realmId
      //realm_id //
    }


  }

  request.post(postBody, function (e, r, data) {
   var  accessToken = qs.parse(data)
    // console.log('realmId ',postBody.realmId)
    // console.log(accessToken)
    // console.log(postBody.oauth)
    
   req.session.qbo = {
      token: accessToken.oauth_token,
      secret: accessToken.oauth_token_secret,
      companyid: postBody.oauth.realmId
    };

    // save the access token somewhere on behalf of the logged in user
     // save the access token somewhere on behalf of the logged in user
    // qbo = new QuickBooks(consumerKey,
    //                      consumerSecret,
    //                      accessToken.oauth_token,
    //                      accessToken.oauth_token_secret,
    //                      realm_id,
    //                      //postBody.oauth.realmId,
    //                      false, // use the Sandbox
    //                      true); // turn debugging on

  qbo = getQbo(req.session.qbo);

    // test out account access
    qbo.findAccounts(function(_, accounts) {
      accounts.QueryResponse.Account.forEach(function(account) {
       // console.log(account.Name)

      })
    })
  })

//res.render('user.ejs',{user:"John Smith"}) 
  res.send('<!DOCTYPE html><html lang="en"><head></head><body><script>window.opener.location.reload(); window.close();</script></body></html>')
  
})

var getQbo = function (args) {
  return new QuickBooks(consumerKey,
                       consumerSecret,
                       args.token,
                       args.secret,
                       args.companyid,
                       false, // use the Sandbox
                       true); // turn debugging on

};
// /* GET quickLogin page. */
// router.get('/start', function(req, res, next) {
//   res.render('start', { title: 'QBO Connect' });
// });

module.exports = router;
