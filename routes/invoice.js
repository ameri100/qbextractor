
var express = require('express'),
request       = require('request'),
cookieSession = require('cookie-session'),
    http          = require('http'),
    port          = process.env.PORT || 3000,
    request       = require('request'),
    qs            = require('querystring'),
    util          = require('util');
var path          = require('path');
var router = express.Router(),
  QuickBooks    = require('../index')
var Excel = require("exceljs");
var fs = require("fs");
var app = express();
var local_reports = '',
    tmpEmployeeInfo = {'key':'value'},
    jsonObj = {'key':'value'},
    columnWidth = 30;

// Render invoices page and get all Invoice Data
router.get('/', function(req,res) {
  //console.log(req.session);

  qbo.findInvoices('', function(err, invoice) {
    var obj = {'key':'value'};
 console.log(
             /* define stringify */
            // JSON.stringify(invoice[0])
 );
  //  console.log(employee);
  // putting invoice data into jsonObj 
    jsonObj = invoice;
    res.render('invoices.ejs', { locals: { invoice: invoice }})
  })
});

// putting all invoice data into excel file (Invoice Items)
router.post("/getSalesInvoice", function(req, res) {

qbo.findInvoices('', function(err, invoice) {
    var obj = {'key':'value'};
 console.log(
             /* define stringify */
             //JSON.stringify(invoice)
 );
  //  console.log(employee);
    //jsonObj = invoice;
    
  })
var options = {
    useStyles: true,
    useSharedStrings: true
  };

   var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet = workbook.addWorksheet("My Sheet");

var salesInvoiceHeader = {
  DocNum : "DocNum",
  LineNum: "LineNum",
  ItemDescription: "Discription",
  Price: "Price",
  AccountCode: "AcctCode",
  TaxCode: "TaxCode",
  LineTotal:"LineTotal",
  LocationCode:"LocationCode",
  CardCode: "CardCode",
  WTLiable:"WTLiable"
};
  
  worksheet.columns = [
    { header: salesInvoiceHeader.DocNum,            key: salesInvoiceHeader.DocNum,      width: columnWidth },
    { header: salesInvoiceHeader.LineNum,           key: salesInvoiceHeader.LineNum,     width: columnWidth },
    { header: salesInvoiceHeader.ItemDescription,   key: salesInvoiceHeader.ItemDescription,    width: columnWidth },
    { header: salesInvoiceHeader.Price,             key: salesInvoiceHeader.Price,    width: columnWidth },
    { header: salesInvoiceHeader.AccountCode,       key: salesInvoiceHeader.AccountCode,     width: columnWidth },
    { header: salesInvoiceHeader.TaxCode,           key: salesInvoiceHeader.TaxCode,  width: columnWidth },
    { header: salesInvoiceHeader.LineTotal,         key: salesInvoiceHeader.LineTotal,     width: columnWidth },
    { header: salesInvoiceHeader.LocationCode,      key: salesInvoiceHeader.LocationCode,    width: columnWidth },
    { header: salesInvoiceHeader.CardCode,          key: salesInvoiceHeader.CardCode, width: columnWidth },
    { header: salesInvoiceHeader.WTLiable,          key: salesInvoiceHeader.WTLiable,   width: columnWidth }
  ];

var key = 'Invoice';

var invoices = {"":""};
invoices =  jsonObj.QueryResponse.Invoice;
//console.log('Length of Employee',invoices.length);

for (var i = 0 ; i < invoices.length ; i++) {
  
var Line_key = 'Line';
  var Lines = invoices[i][Line_key];
//console.log('Length of Employee',Lines);
  for (var j = 0; j < Lines.length ; j++) {
    worksheet.addRow({
    // JournalMemo data not found in QuickBooks
     DocNum: invoices[i].DocNumber,
     LineNum: Lines[j].LineNum,
     ItemDescription: Lines[j].Description,
     Price: Lines[j].Amount,
     AccountCode: '',
     TaxCode : 'zero tax code',
     LineTotal: Lines[j].Amount,
     LocationCode: '',
     CardCode: invoices[i].CustomerRef.name,
     WTLiable: 'tNo'
      
  }).commit;
  };
  

};
  
  worksheet.commit();
  workbook.commit();
});

// putting all invoice data into excel file (Invoice Items)
router.post("/getInvoice", function(req, res) {

qbo.findInvoices('', function(err, invoice) {
    var obj = {'key':'value'};
 console.log(
             /* define stringify */
           //  JSON.stringify(invoice)
 );
  //  console.log(employee);
    jsonObj = invoice;
    
  })
var options = {
    useStyles: true,
    useSharedStrings: true
  };

   var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
  workbook.zip.pipe(res);
  var worksheet = workbook.addWorksheet("My Sheet");

var invoiceHeader = {
  DocNum : "DocNum",
  DocType: "DocType",
  CardCode: "CardCode",
  CardName: "CardName",
  DocDate: "DocDate",
  DocDueDate: "DocDueDate",
  TaxDate: "TaxDate",
  Comments:"Comments",
  JournalMemo:"JrnlMemo",
  NumAtCard: "NumAtCard",
  ControlAccount:"CtlAccount",
  Currency:"Currency",
  Rate:"Rate"
};
  
  worksheet.columns = [
    { header: invoiceHeader.DocNum,     key: invoiceHeader.DocNum,      width: columnWidth },
    { header: invoiceHeader.DocType,    key: invoiceHeader.DocType,     width: columnWidth },
    { header: invoiceHeader.CardCode,   key: invoiceHeader.CardCode,    width: columnWidth },
    { header: invoiceHeader.CardName,   key: invoiceHeader.CardName,    width: columnWidth },
    { header: invoiceHeader.DocDate,    key: invoiceHeader.DocDate,     width: columnWidth },
    { header: invoiceHeader.DocDueDate, key: invoiceHeader.DocDueDate,  width: columnWidth },
    { header: invoiceHeader.TaxDate,    key: invoiceHeader.TaxDate,     width: columnWidth },
    { header: invoiceHeader.Comments,         key: invoiceHeader.Comments,    width: columnWidth },
    { header: invoiceHeader.JournalMemo,      key: invoiceHeader.JournalMemo, width: columnWidth },
    { header: invoiceHeader.NumAtCard,        key: invoiceHeader.NumAtCard,   width: columnWidth },
    { header: invoiceHeader.ControlAccount,   key: invoiceHeader.ControlAccount,  width: columnWidth },
    { header: invoiceHeader.Currency,         key: invoiceHeader.Currency,        width: columnWidth },
    { header: invoiceHeader.Rate,             key: invoiceHeader.Rate,            width: columnWidth }
  ];

var key = 'Invoice';
var invoices = jsonObj.QueryResponse[key];
// console.log('Length of Employee',invoices.length);

for (var i = 0 ; i < invoices.length ; i++) {
  

  
  worksheet.addRow({
    // JournalMemo data not found in QuickBooks
     DocNum: invoices[i].DocNumber,
      DocType: 'dDocument_Items',
      DocDueDate : invoices[i].DueDate,
      CardName:  invoices[i].CustomerRef.name,
      DocDate: invoices[i].TxnDate,
      Comments : invoices[i].PrivateNote,
      Currency: invoices[i].CurrencyRef.value,
      Rate: "1"
     
  }).commit;

};
  
  worksheet.commit();
  workbook.commit();
});


module.exports = router;